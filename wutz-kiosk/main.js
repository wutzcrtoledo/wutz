const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
const ipcMain = require('electron').ipcMain;
let popUps = {};

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 1500, height: 700})

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  mainWindow.setKiosk(true);


  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

//-- Custom Function to handle multiple windows
function createPopUp (name, contPath, options, callback) {
  if(typeof options === "function"){
     callback = options;
     options = null;
  }
  options = options||{width: 800, height: 800};

  // Create the browser window.

    popUps[name] = new BrowserWindow(options);
    popUps[name].loadURL('file://' + __dirname + contPath);
    popUps[name].on('closed', function () {
      popUps[name] = null;
    });

    console.log(electron.screen.getAllDisplays());

  if(callback)
     callback(popUps[name]);
}

function createPopUpOnExtScreen (name, contPath, options, callback) {
  if(typeof options === "function"){
     callback = options;
     options = null;
  }
  options = options||{width: 800, height: 800};

  let displays = electron.screen.getAllDisplays()
  let externalDisplay = displays.find((display) => {
      return display.bounds.x !== 0 || display.bounds.y !== 0
  })

  if (externalDisplay) {
      options.x = externalDisplay.bounds.x + 50;
      options.y = externalDisplay.bounds.y + 50;
  }

  // Create the browser window.

    popUps[name] = new BrowserWindow(options);
    popUps[name].loadURL('file://' + __dirname + contPath);
    popUps[name].setKiosk(true);
    popUps[name].on('closed', function () {
      popUps[name] = null;
    });

  if(callback)
     callback(popUps[name]);
}

ipcMain.on('openPopUp', (event, arg) => {
  //event.returnValue = app.getAppPath();
     if(!popUps || !popUps[arg.name]){
          console.log("---------------------------------");
          console.log(arg);
          // createPopUp (arg.name, arg.contPath, arg.options, (_win)=>{
          createPopUpOnExtScreen (arg.name, arg.contPath, arg.options, (_win)=>{
               event.sender.send('whereIsMyPopUp', _win);
           });
     }
     else if(popUps){
        popUps[arg.name].focus();
     }
});

ipcMain.on('sendMsg2Win', (event, arg) => {
  //event.returnValue = app.getAppPath();
  var name = arg.send2;
  var inter = setInterval(function(){
      if(popUps && popUps[name] && popUps[name].ready){
         console.log("Waiting "+name+" Window to Load");
         clearInterval(inter);
         popUps[name].webContents.send('win-request',{sessToken:arg.sessToken,value:arg.value});
      }
  },200);
});

ipcMain.on('showFocusWin', (event, arg) => {
  //event.returnValue = app.getAppPath();
  var name = arg.winId;
  if(name === "main"){
     mainWindow.show();
     mainWindow.focus();
     return ;
  }
  var inter = setInterval(function(){
      if(popUps && popUps[name] && popUps[name].ready){
         clearInterval(inter);
         popUps[name].show();
         popUps[name].focus();
      }
  },200);
});

ipcMain.on('markWinReady', (event, arg) => {
  //event.returnValue = app.getAppPath();
  var name = arg.winName;
  popUps[name]["ready"] = true;
});

ipcMain.on('kioskModeOFF', (event, arg) => {
  //event.returnValue = app.getAppPath();
  var name = arg.winName;
  popUps[name].setKiosk(false);
});

ipcMain.on('printWindow', (event, arg) => {
  //event.returnValue = app.getAppPath();
  var name = arg.winName;
  popUps[name].webContents.print({silent:true, printBackground:true});
});
