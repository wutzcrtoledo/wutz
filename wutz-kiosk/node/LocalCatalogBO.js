var _ = require("underscore");
var sessInfo;

var artists;
var albums;
var songs;

var loadLoadedCatalog = function(){
   artists = JSON.parse(window.sessionStorage.getItem("session_artists"));
   albums = JSON.parse(window.sessionStorage.getItem("session_albums"));
   songs = JSON.parse(window.sessionStorage.getItem("session_songs"));
};

var getArtist = function(_artId){
  if(!artists)
      loadLoadedCatalog();
  return _.find(artists, {idartist:_artId});
};

var getAlbum = function(_albId, _artId){
  if(!albums)
    loadLoadedCatalog();
  return _artId?(_.find(albums[_artId],{idalbum:_albId})):_.find(albums, {idalbum:_albId});
};

var getSong = function(_sngId, _albId){
  if(!songs)
    loadLoadedCatalog();
  return _albId?(songs[_albId][_sngId]):_.find(songs, {songid:_sngId});
};

var getAllSongsPerArtist = function(_artId){
    if(!artists)
        loadLoadedCatalog();

    console.log("Filtering");

    var _songs = [];
    for(var i in albums[_artId]){
        _songs = _.union(_songs, songs[albums[_artId][i].idalbum]);
        //_songs.push(albums[_artId][i].idalbum);
    }
  //  console.log(_songs);
    return _songs;
};

var filterArtists = function(_crit){
    if(!artists)
        loadLoadedCatalog();

    return _.filter(artists, function(node){
                                  return (node.name.toLowerCase()).indexOf(_crit.toLowerCase())!==-1;
                         });
};

var filterAlbums = function(_albList, _crit){
    if(!artists)
        loadLoadedCatalog();

        return _.filter(_albList, function(node){
                                      return (node.name.toLowerCase()).indexOf(_crit.toLowerCase())!==-1;
                             });
};

var filterSongs = function(_sngList,_crit){
    if(!artists)
        loadLoadedCatalog();

        return _.filter(_sngList, function(node){
                                      return (node.name.toLowerCase()).indexOf(_crit.toLowerCase())!==-1;
                             });
};

module.exports = {
    getAllSongsPerArtist: getAllSongsPerArtist,
    filterArtists: filterArtists,
    filterAlbums: filterAlbums,
    filterSongs: filterSongs,
    getArtist: getArtist,
    getAlbum: getAlbum,
    getSong: getSong
};
