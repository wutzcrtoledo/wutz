var ajx = require("./misc/AjaxAdmin");
var remCat = require("./RemoteCatalogBO");
var os = require('os');
var wutzUserPath = os.homedir()+"/.wutz";
var fs = require("fs");

var loadConfigFile = function(callback){
  var fileCont = fs.readFileSync(wutzUserPath+"/json/config.json");
  if(!fileCont){
      callback({err: "no_wutz"});
      return false;
  }
  callback(JSON.parse(fileCont));
};
var loadKioskConfigFile = function(callback){
    var fileCont = fs.readFileSync(__dirname+"/../json/kiosk-config.json");
    if(!fileCont){
        callback({err:"not_found"});
        return false;
    }
    callback(JSON.parse(fileCont));
};

var login = function(_accs, callback){
    console.log(_accs);
    ajx.callService("login",{method:"POST",data:_accs},function(_res){
        console.log("callback");
        callback(_res);
        if(_res.logged)
              sessionCallback(_res);
    });
};

var refresh = function(sessToken, callback){
  //  console.log(_accs);
    ajx.callService("refresh",{method:"POST",data:{"sessToken":sessToken}},callback);
};


var sessionCallback = function(_loggedInfo,callback){
    var filConf = JSON.parse(fs.readFileSync(wutzUserPath+"/json/config.json"));
    var sessionInfo = (window.localStorage.getItem("session_"+_loggedInfo.barDet.id))?JSON.parse(window.localStorage.getItem("session_"+_loggedInfo.barDet.id)):{};
    if(_loggedInfo.accTkn){
        sessionInfo.accTkn = _loggedInfo.accTkn;
    }
    window.localStorage.setItem("session_"+_loggedInfo.barDet.id,JSON.stringify(sessionInfo));

    _loggedInfo.barDet["guid"] = filConf.guid;
    window.sessionStorage.setItem("session_info",JSON.stringify({barInfo:_loggedInfo.barDet,token:_loggedInfo.token}));

    remCat.connect2Bar(function(_res){
        console.log(_res);
        if(callback)
          callback({"loginOK":true,"bar":_loggedInfo.barDet});
    });
};

var loadBarData = function(callback){
     loadConfigFile((_conf)=>{
        if(_conf.err){
            callback(_conf);
            return ;
        }
        if(_conf.bar_id && window.localStorage.getItem("session_"+_conf.bar_id)){
              var svSess = JSON.parse(window.localStorage.getItem("session_"+_conf.bar_id));
              login({"barid":_conf.bar_id,"accTkn":svSess.accTkn},(_res)=>{
                    if(_res.logged){
                        sessionCallback(_res, callback);
                    }
                    else{
                        callback({err:"no_login",barid:_conf.bar_id});
                    }
              });
        }
        else{
          callback({err:"no_login",barid:_conf.bar_id});
        }
     });
};

var indexCatalog = function(callback){
    remCat.indexCatalog(callback);
};


var init = function(callback){
     console.log("Starting App");
     loadBarData(callback);
};


module.exports = {
    init: init,
    login: login,
    refresh: refresh,
    indexCatalog: indexCatalog, 
    loadKioskConfigFile: loadKioskConfigFile
};
