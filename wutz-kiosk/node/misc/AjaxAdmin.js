//Private
 var  _config = {
    adminHost : 'https://wutzapi.bluezinc.cl'
   //	adminHost : 'http://localhost/WutzAdmin'
 };
//Public
   var callService = function(service,params,callback){
           if(typeof params === "function"){
               callback = params;
           }
           method = params.method||"GET";
           authToken = params.authToken||"";
           params = JSON.stringify(params.data||"");
           $.ajax({
                   type: method,
                   dataType: 'json',
                   url: _config.adminHost+"/"+service,
                   headers: { 'Authorization': authToken },
                   data: params,
                   success: function (result) {
                           callback(result);
                   },
                   error: function (xhr, txtStat, errThrown) {
                           var err = {};
                           err.xhr = xhr;
                           err.txtStat = txtStat;
                           err.errThrown = errThrown;
                           callback(err);
                   }
          });
   };

   module.exports = {
       callService: callService
   };
