var ajx = require("./misc/AjaxAdmin");
var _ = require("underscore");
var sessInfo;

var loadSessInfo = function(){
     sessInfo = JSON.parse(window.sessionStorage.getItem("session_info"));
};


var addCredit = function(callback){
    //if(!sessInfo)
    loadSessInfo();

    var params = {method:"POST",
                  authToken:sessInfo.token};
    var _data = {catId:sessInfo.barInfo.idcatalog,
                 barId:sessInfo.barInfo.id,
                 guid:sessInfo.barInfo.guid,
                 credits:1};
    params.data = _data;

    ajx.callService("addCredits",params,callback);
};

var refreshCredit = function(callback){
    //if(!sessInfo)
    loadSessInfo();

    var params = {method:"POST",
                  authToken:sessInfo.token};
    var _data = {catId:sessInfo.barInfo.idcatalog,
                 barId:sessInfo.barInfo.id,
                 guid:sessInfo.barInfo.guid,
                 credits:0};
    params.data = _data;

    ajx.callService("addCredits",params,callback);
};

var getTransferCode = function(_cred, callback){
  //  if(!sessInfo)
    loadSessInfo();

    var params = {method:"POST",
                  authToken:sessInfo.token};
    var _data = {catId:sessInfo.barInfo.idcatalog,
                 guid:sessInfo.barInfo.guid,
                 credits:_cred};
    params.data = _data;

    ajx.callService("createTransfCode",params,callback);
};

module.exports = {
    addCredit: addCredit,
    getTransferCode: getTransferCode,
    refreshCredit: refreshCredit
};
