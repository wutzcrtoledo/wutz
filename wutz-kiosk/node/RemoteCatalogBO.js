var ajx = require("./misc/AjaxAdmin");
var _ = require("underscore");
var sessInfo;


var loadArtists = function(callback){
  ajx.callService("getArtistList/"+sessInfo.barInfo.idcatalog,function(_res){
      _res.forEach((val)=>{
        if(val.lfm_img_url){
            val.img = new Image()
            val.img.src = val.lfm_img_url;
        }
      });
      callback(_res);
  });
};

var loadAlbums = function(art,callback){
   ajx.callService("getAlbumPerArtist/"+sessInfo.barInfo.idcatalog+"/"+art.idartist,function(_res){
       _res.forEach((val)=>{
         if(val.lfm_img_url){
             val.img = new Image()
             val.img.src = val.lfm_img_url;
         }
       });
       callback(art.idartist, _res);
   });
};

var loadSongs = function(alb,callback){
    //http://localhost:3000/proxy/http%3A%2F%2Fwutz.co.uk/getSongsPerAlbum/ 353 / 46006
    ajx.callService("getSongsPerAlbum/"+sessInfo.barInfo.idcatalog+"/"+alb.idalbum,function(_res){
        callback(alb.idalbum, _res);
    });
};

var indexCatalog = function(callback){
  sessInfo = JSON.parse(window.sessionStorage.getItem("session_info"));
  var _perc = 10;
  var artLoaded = false,albumsLoaded = false, songsLoaded = false;
  var albumsStarted = false, songsStarted = false;
  var artists = [];
  var albums = {};
  var songs = {};

  if(window.sessionStorage.getItem("session_artists")){
      artists = JSON.parse(window.sessionStorage.getItem("session_artists"));
      albums = JSON.parse(window.sessionStorage.getItem("session_albums"));
      songs = JSON.parse(window.sessionStorage.getItem("session_songs"));
      _perc = 100;
      callback({perc:_perc,finished:true,arts:artists,albs:albums,sngs:songs});
      return ;
  }


  loadArtists(function(_artists){
          artists = _artists;
          artLoaded = true;
          _perc = 20;
          callback({perc:_perc,arts:artists});
  });

  var checkInter = setInterval(()=>{
      if(artLoaded && albumsLoaded && songsLoaded){
          window.sessionStorage.setItem("session_artists",JSON.stringify(artists));
          window.sessionStorage.setItem("session_albums",JSON.stringify(albums));
          window.sessionStorage.setItem("session_songs",JSON.stringify(songs));
          _perc = 100;
          callback({perc:_perc,finished:true,arts:artists,albs:albums,sngs:songs});
          clearInterval(checkInter);
          return ;
      }
      else if(artLoaded && albumsLoaded && !songsStarted){
          songsStarted = true;
          _perc = 70;
          callback({perc:_perc});
          //71 - 99
          var ping = 0, pong = 0;

          var _albCouObj = _.countBy(albums, function(_obj) {
                                                  return _obj.length;
                                                });
          console.log(_albCouObj);
          var tot = 0;
          for(var i in _albCouObj){
              tot += parseInt(i)*_albCouObj[i];
          }
          console.log(tot);
          var fact = Math.round(tot/(99-71)),factCounter = 0;
        //  console.log(JSON.stringify(albums));
        //  console.log(JSON.stringify(_.values(albums)));
         console.log("Fact Albums "+fact);
          for(var artId in albums){
              for(var j in albums[artId]){
                var albNode = albums[artId][j];
                songs[albNode.idalbum] = [];
                ping++;
                loadSongs(albNode,(_albId, _songs)=>{
                    factCounter++;
                    if(factCounter >= fact){
                       factCounter = 0;
                       _perc++;
                       callback({perc:_perc});
                    }
                   pong++;
                   songs[_albId] = _songs;
                   songsLoaded = (ping === pong);
                });
              }
          }
      }
      else if(artLoaded && !albumsStarted){
          albumsStarted = true;
          _perc = 25;
          callback({perc:_perc});
          //25 - 70
          //Loading ALbums
          console.log(JSON.stringify(albums));
          var ping = 0, pong = 0;
          var fact = Math.round(artists.length/(70-25)),factCounter = 0;
          console.log("Fact Artists "+fact);
          for(var i in artists){
             albums[artists[i].idartist] = [];
             ping++;
             loadAlbums(artists[i],(_artId, _albums)=>{
                 factCounter++;
                 if(factCounter >= fact){
                    factCounter = 0;
                    _perc++;
                    callback({perc:_perc});
                 }
                 pong++;
                 albums[_artId] = _albums;
                 albumsLoaded = (ping === pong);
             });
          }
      }
  },2000);
};

var addSongToQueue = function(_songId, callback){
  sessInfo = JSON.parse(window.sessionStorage.getItem("session_info"));
  var params = {method:"POST",
                authToken:sessInfo.token};
  var _data = {catId:sessInfo.barInfo.idcatalog,
               guid:sessInfo.barInfo.guid,
               token:sessInfo.barInfo.dayToken,
               songId:_songId
             };
  params.data = _data;
  ajx.callService("addSongToQueue",params,callback);
};

var connect2Bar = function(callback){
  sessInfo = JSON.parse(window.sessionStorage.getItem("session_info"));
  var params = {method:"POST",
                authToken:sessInfo.token};
  var _data = {catId:sessInfo.barInfo.idcatalog,
               guid:sessInfo.barInfo.guid,
               token:sessInfo.barInfo.dayToken
             };
  params.data = _data;
  ajx.callService("connect2Bar",params,callback);
};

var getCurrentPlayList = function(callback){
  sessInfo =  JSON.parse(window.sessionStorage.getItem("session_info"));
  var params = {method:"POST"};
  var _data = {catId:sessInfo.barInfo.idcatalog,
               token:sessInfo.barInfo.dayToken};
  params.data = _data;
  ajx.callService("getCurrentPlayList",params,callback);
};

//-- Player

var getAllCatalogPlayList = function(callback){
   sessInfo =  JSON.parse(window.sessionStorage.getItem("session_info"));
   var _reqbody = {catId:sessInfo.barInfo.idcatalog,
                   token:sessInfo.barInfo.dayToken
                 };
   ajx.callService("getFullCatalog",{method:"POST",data: _reqbody, authToken:sessInfo.token},callback);
};

var rescAndGetPendingListFromLastSess = function(callback){
   var me = this;
   console.log(sessInfo);
   sessInfo =  JSON.parse(window.sessionStorage.getItem("session_info"));
   var _reqbody = {catId:sessInfo.barInfo.idcatalog,
                   token:sessInfo.barInfo.dayToken
                 };
   ajx.callService("rescuePendingList",{method:"POST",data: _reqbody, authToken:sessInfo.token},()=>{
         me.getAllCatalogPlayList(callback);
   });
};

var removeSongFromPlaylist = function(_songid,_cli_guid,callback){
   sessInfo =  JSON.parse(window.sessionStorage.getItem("session_info"));
   var _reqbody = {catId:sessInfo.barInfo.idcatalog,
                   token:sessInfo.barInfo.dayToken,
                   songId: _songid,
                   guid: _cli_guid
                 };
   ajx.callService("unqueueSong",{method:"POST",data: _reqbody, authToken:sessInfo.token},callback);
};

var addRandomSong = function(callback){
    var me = this;
    me.addSongToQueue(-1, callback);
};

module.exports = {
    indexCatalog: indexCatalog,
    addSongToQueue: addSongToQueue,
    connect2Bar: connect2Bar,
    getCurrentPlayList: getCurrentPlayList, //
    getAllCatalogPlayList: getAllCatalogPlayList,
    rescAndGetPendingListFromLastSess: rescAndGetPendingListFromLastSess,
    removeSongFromPlaylist: removeSongFromPlaylist,
    addRandomSong: addRandomSong
};
