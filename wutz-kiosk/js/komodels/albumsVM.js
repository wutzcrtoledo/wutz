// Main viewmodel class
define(['knockout',
        'bootstrap',
        'i18n!nls/nls.example.js',
        'i18nTool',
        'arrTool',
        'masonry.pkgd',
        'text!'+dom+'/templates/albums.html',
        'inspinia',
        'bootstrap2-toggle'],
  function(ko, bt,i18nAtt,atTool, arrT ,Masonry  ,htmTlp) {
    return function imagesModVM() {
      var mainMod = this;
      //Private
      var locCatBo = require(__dirname+"/node/LocalCatalogBO");
      var initJS = function(){
        if(koMods.app.kioskMode()){
            $(".contentSection").kinetic({cursor:"none",
                                          invert: true,
                                          filterTarget: function(target, e){
                                                                    if (!/down|start/.test(e.type)){
                                                                        return !(/area|a|input|button/i.test(target.tagName) || /contBox/ig.test($(target).attr("class")));
                                                                    }}
                                        });
            $("body").css("cursor","none");
        }
      };

      //Public
        mainMod.welcomeMsg = ko.observable("Welcome To Main Albums Model");
        mainMod.template = htmTlp;
        mainMod.locale = function(attrName, keys){
           return atTool.translateAtt(attrName, i18nAtt[attrName], keys);
        };
        mainMod.visible = ko.observable(false);
        mainMod.loaded = ko.observable(false);
        mainMod.flowClass = ko.observable("animated fadeInRightBig");
        mainMod.allAlbums = ko.observable();
        mainMod.selArtist = ko.observable();
        mainMod.artAlbums = ko.observableArray([]);
        mainMod.filterBox = ko.observable("");
        mainMod.onkoModAppear = function(){
              console.log("Albums VM  Started");
              initJS();
              var albList = mainMod.allAlbums()[mainMod.selArtist().idartist];
              mainMod.artAlbums(albList);
        };

        mainMod.loadSongs = function(albNode){
          koMods['songs'].selArtist(mainMod.selArtist());
          koMods['songs'].selAlbum(albNode);
          koMods['app'].loadMainModule('songs', 'Right');
        };

        mainMod.loadAllSong = function(){
          koMods['songs'].selArtist(mainMod.selArtist());
          koMods['songs'].selAlbum(null);
          koMods['app'].loadMainModule('songs', 'Right');
        };

        mainMod.applyFilter = function(data,ev){
            var char2Add = $(ev.currentTarget).html()!=="_"?$(ev.currentTarget).html():" ";
            var filBox = mainMod.filterBox();
            console.log(char2Add);
            console.log(filBox);
            filBox+=char2Add;
            mainMod.filterBox(filBox);
            mainMod.artAlbums(locCatBo.filterAlbums(mainMod.allAlbums()[mainMod.selArtist().idartist], filBox));
          //  console.log(data);
        };

        mainMod.removeFilter = function(){
            var filBox = mainMod.filterBox();
            filBox = filBox.substr( 0 , filBox.length-1 );
            mainMod.filterBox(filBox);
            if(filBox === "")
                mainMod.artAlbums(mainMod.allAlbums()[mainMod.selArtist().idartist]);
            else
                mainMod.artAlbums(locCatBo.filterAlbums(mainMod.allAlbums()[mainMod.selArtist().idartist], filBox));
        };

//-- Finish
    };
 }
);
