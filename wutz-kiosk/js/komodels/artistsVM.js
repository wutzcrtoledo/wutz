// Main viewmodel class
define(['knockout',
        'bootstrap',
        'i18n!nls/nls.example.js',
        'i18nTool',
        'arrTool',
        'masonry.pkgd',
        'text!'+dom+'/templates/artists.html',
        'inspinia',
        'bootstrap2-toggle'],
  function(ko, bt,i18nAtt,atTool, arrT ,Masonry  ,htmTlp) {
    return function imagesModVM() {
      var mainMod = this;
      //Private
      var locCatBo = require(__dirname+"/node/LocalCatalogBO");
      var initJS = function(){
        
        if(koMods.app.kioskMode()){
            $(".contentSection")
                .kinetic({cursor:"none",
                        invert: true,
                        filterTarget: function(target, e){
                            if (!/down|start/.test(e.type)){
                                return !(/area|a|input|button/i.test(target.tagName) || /contBox/ig.test($(target).attr("class")));
                            }
                            }
                });
            $("body").css("cursor","none");
         }
      };

      //Public
        mainMod.welcomeMsg = ko.observable("Welcome To Main Artists Model");
        mainMod.template = htmTlp;
        mainMod.locale = function(attrName, keys){
           return atTool.translateAtt(attrName, i18nAtt[attrName], keys);
        };
        mainMod.visible = ko.observable(false);
        mainMod.loaded = ko.observable(false);
        mainMod.flowClass = ko.observable("animated fadeInRightBig");
        mainMod.artList = ko.observableArray([]);
        mainMod.filterBox = ko.observable("");
        mainMod.onkoModAppear = function(){
              console.log("Artists VM  Started");
              initJS();

        };
        mainMod.loadAlbums = function(_art){
            koMods['albums'].selArtist(_art);
            koMods['app'].loadMainModule('albums', 'Right');
        };

        mainMod.applyFilter = function(data,ev){
            var char2Add = $(ev.currentTarget).html()!=="_"?$(ev.currentTarget).html():" ";
            var filBox = mainMod.filterBox();
            console.log(char2Add);
            console.log(filBox);
            filBox+=char2Add;
            mainMod.filterBox(filBox);
            mainMod.artList(locCatBo.filterArtists(filBox));
          //  console.log(data);
        };

        mainMod.removeFilter = function(){
            var filBox = mainMod.filterBox();
            filBox = filBox.substr( 0 , filBox.length-1 );
            mainMod.filterBox(filBox);
            if(filBox === "")
                mainMod.artList(JSON.parse(window.sessionStorage.getItem("session_artists")));
            else
                mainMod.artList(locCatBo.filterArtists(filBox));
        };

        mainMod.getArtist  = function(_artId){
          return locCatBo.getArtist(_artId);
        };

//-- Finish
    };
 }
);
