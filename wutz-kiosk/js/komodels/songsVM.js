// Main viewmodel class
define(['knockout',
        'bootstrap',
        'i18n!nls/nls.example.js',
        'i18nTool',
        'arrTool',
        'masonry.pkgd',
        'text!'+dom+'/templates/songs.html',
        'inspinia',
        'bootstrap2-toggle'],
  function(ko, bt,i18nAtt,atTool, arrT ,Masonry  ,htmTlp) {
    return function imagesModVM() {
      var mainMod = this;
      //Private
      var locCatBo = require(__dirname+"/node/LocalCatalogBO");
      var remCatBo = require(__dirname+"/node/RemoteCatalogBO");
      var backDispSng = [];

      var initJS = function(){
            if(koMods.app.kioskMode()){
                    $(".contentSection").kinetic({cursor:"none",
                                                invert: true,
                                                filterTarget: function(target, e){
                                                        console.log(target);
                                                        // console.log($(".contBox", target).length);
                                                        console.log(e);
                                                        if (!/down|start/.test(e.type)){
                                                                return !(/area|a|input|button/i.test(target.tagName) || /contBox/ig.test($(target).attr("class")));
                                                        }}
                                                });
                    $("body").css("cursor","none");
            }
      };

      //Public
        mainMod.welcomeMsg = ko.observable("Welcome To Main Songs Model");
        mainMod.template = htmTlp;
        mainMod.locale = function(attrName, keys){
           return atTool.translateAtt(attrName, i18nAtt[attrName], keys);
        };
        mainMod.visible = ko.observable(false);
        mainMod.loaded = ko.observable(false);
        mainMod.flowClass = ko.observable("animated fadeInRightBig");
        mainMod.allSongs = ko.observable();
        mainMod.selArtist = ko.observable();
        mainMod.selAlbum = ko.observable();
        mainMod.displaySongs = ko.observableArray([]);
        mainMod.filterBox = ko.observable("");

        mainMod.onkoModAppear = function(){
              console.log("songs VM  Started");
              var dispSongs;
              if(mainMod.selAlbum())
                   dispSongs = mainMod.allSongs()[mainMod.selAlbum().idalbum];
              else if(mainMod.selArtist()){
                    var locCat = require(__dirname+"/node/LocalCatalogBO");
                   dispSongs = locCat.getAllSongsPerArtist(mainMod.selArtist().idartist);
              }

            //  console.log(JSON.stringify(mainMod.allSongs()));
              backDispSng = dispSongs;
              mainMod.displaySongs(dispSongs);
              initJS();
        };

        mainMod.applyFilter = function(data,ev){
            var char2Add = $(ev.currentTarget).html()!=="_"?$(ev.currentTarget).html():" ";
            var filBox = mainMod.filterBox();
            console.log(char2Add);
            console.log(filBox);
            filBox+=char2Add;
            mainMod.filterBox(filBox);
            mainMod.displaySongs(locCatBo.filterSongs(backDispSng, filBox));
          //  console.log(data);
        };

        mainMod.removeFilter = function(){
            var filBox = mainMod.filterBox();
            filBox = filBox.substr( 0 , filBox.length-1 );
            mainMod.filterBox(filBox);
            if(filBox === "")
                mainMod.displaySongs(backDispSng);
            else
                mainMod.displaySongs(locCatBo.filterSongs(backDispSng, filBox));
        };

        mainMod.addSong2Queue = function(sngNode){
            var currCred =  koMods.app.credits();
            if(currCred > 0){
                koMods.app.loading(true);
                remCatBo.addSongToQueue(sngNode.songid, function(_res){
                    koMods.app.loading(false);
                    if(_res.added === "OK"){
                        currCred--;
                        koMods.app.credits(currCred);
                        koMods.app.messageBox("OK","El tema \""+sngNode.name+"\" fue agregado","primary");
                        return ;
                    }
                    if(_res.msg === "repeated")
                        koMods.app.messageBox("No OK","El tema \""+sngNode.name+"\" ya está en la cola","danger");
                    else
                        koMods.app.messageBox("No OK","El tema \""+sngNode.name+"\" no pudo ser encolado. Revise si tiene crédito","danger");

                    return ;
                });
            }
            else{
               koMods.app.messageBox("No OK","No tienes créditos suficientes para encolar temas","warning");
            }
        };

        mainMod.getCurrentPlayList = function(limit,callback){
            var _ = require("underscore");
            if(typeof(limit) === "function"){
                  callback = limit;
                  limit = null;
            }
            remCatBo.getCurrentPlayList((_res)=>{
              //console.log("Check limit");
              var totCount = _res.length;
              _res = !limit?_res:_.last(_res, limit||3);
              var song2Load = [];
              totCount = totCount - _res.length;
              for(var i in _res){
                var sng = _res[i];
                sng.queueIdx = totCount;
                totCount++;
                if(sng.idalbum){
                   var alb = locCatBo.getAlbum(sng.idalbum, sng.idartist);
                   sng.pic = alb.lfm_img_url;
                }
                if(!sng.pic){
                  var artNode = koMods.artists.getArtist(sng.idartist);
                  sng.pic = artNode?artNode.lfm_img_url:null;
                }
                song2Load.push(sng);
               }
               callback(song2Load);
            });
        };

//-- Finish
    };
 }
);
