function playerMod() {
    var mainMod = this;
    //Private
    var remCatBo = require("../../node/RemoteCatalogBO");
    var localCatBo = require("../../node/LocalCatalogBO");
    var _ = require("underscore");
    var ipcRenderer = require('electron').ipcRenderer;
    var _winName = "playerWin";
    var loadSessionVariables = function(_sesParams, _skipInit){
        for(var key in _sesParams)
             window.sessionStorage.setItem(key, JSON.stringify(_sesParams[key]));
        if(!_skipInit)
            mainMod.init();
    };
    ipcRenderer.on('win-request', (event, arg) => {
          if(arg.value.refreshSessInfo){
            loadSessionVariables(arg.value.sessVar, true);
            return ;
          }
          loadSessionVariables(arg.value);
    });
    ipcRenderer.send('markWinReady', {winName:_winName});
    console.log("Window Loaded "+_winName);

    const initMainListener = function(){
        $("body").click(function(){
            ipcRenderer.send('showFocusWin', {winId:"main"});
        });
    };
//------------------------------ Player ---------------------------
   var streamPlaying;
   var streamVideoPlaying;
   const playSong = function(_tries){
    var tries = _tries||0;
    try{
          streamPlaying = AUDIO.VISUALIZER.getInstance({
                               autoplay: true,
                               loop: false,
                               audio: 'myAudio',
                               canvas: 'myCanvas',
                               style: 'lounge',
                               barWidth: 2,
                               barHeight: 2,
                               barSpacing: 7,
                               barColor: '#cafdff',
                               shadowBlur: 20,
                               shadowColor: '#ffffff',
                               font: ['12px', 'Helvetica']
                           });

       setTimeout(()=>{
          var inter = setInterval(function(){
             if(streamPlaying.isPlaying){
                clearInterval(inter);
                monitorPlayingSong();
             }
          },400);
       },400);
     }catch(e){
         console.log("Error Playing Song: " + e);
         setTimeout(()=>{
             if(tries <= 5)
                 playSong(tries+1);
             else
                 locateNextSong();

         },400);
     }
   };

   const playVideo = function(_tries){
      var tries = _tries||0;
      streamVideoPlaying = $("#myVideo").get(0);
      //streamPlaying.requestFullscreen();
        try{
          setTimeout(function(){
            streamVideoPlaying.play();
            $("#myVideo").addClass("fullscreenvideo");
            monitorPlayingVideo();
          },400);
       }
       catch(e){
          console.log("Error Playing Video: " + e);
          setTimeout(()=>{
              if(tries <= 5)
                  playVideo(tries+1);
              else
                  locateNextSong();

          },400);
       }
   };

    const add2PlayList = function(_newsngs){
        for(var i in _newsngs){
           var alb = localCatBo.getAlbum(_newsngs[i].idalbum, _newsngs[i].idartist)||{};
           _newsngs[i].lfm_img_url = alb.lfm_img_url;
           if(!_newsngs[i].lfm_img_url || _newsngs[i].lfm_img_url === ""){
              var art = localCatBo.getArtist(_newsngs[i].idartist)||{};
              _newsngs[i].lfm_img_url = art.lfm_img_url||"../../img/icon.png";
           }
        }
        mainMod.playList(_.union(mainMod.playList(),_newsngs));
      //  console.log(mainMod.playList());
        if(mainMod.playList().length <= 0)
             locateNextSong();
    };

    const locateNextSong = function(){
    //  stopMonitoring();
      if(mainMod.playingSong()){
        remCatBo.removeSongFromPlaylist(mainMod.playingSong().songid,mainMod.playingSong().client_guid,(_res)=>{
            console.log(_res);
            //mainMod.playingSong(null);
        });
      }
      if(mainMod.playingVideo()){
         remCatBo.removeSongFromPlaylist(mainMod.playingVideo().songid,mainMod.playingVideo().client_guid,(_res)=>{
            console.log(_res);
            //mainMod.playingVideo(null);
         });
      }
      if(mainMod.playList().length > 0){
           var playLstUpd = mainMod.playList();
           var song = (playLstUpd.splice(0,1))[0];
           song.lfm_img_url_big = null;
           if(song.album_info){
              var albInfo = JSON.parse(song.album_info);
              var labCovs = _.indexBy(albInfo.album.image,"size");
              song.lfm_img_url_big = labCovs.extralarge||labCovs.large;
              song.lfm_img_url_big = song.lfm_img_url_big["#text"];
           }
           mainMod.playList(playLstUpd);
           mainMod.playingSong(song);
           if(song.media_type !== "video")
               setTimeout(()=>{playSong();},400);
           else
               setTimeout(()=>{playVideo();},400);
       }
       else{
           remCatBo.addRandomSong((_res)=>{
               console.log(_res);
           });
       }
      // initMonitors();
    };

    const monitorPlayingSong = function(){
    //  if(!streamPlaying.isPlaying && parseFloat(streamPlaying.analyser.context.currentTime) > 0){
      if(streamPlaying && !streamPlaying.isPlaying){
          streamPlaying = null;
          locateNextSong();
      }
      else{
         setTimeout(function(){
            monitorPlayingSong();
         },5000);
      }
    };

    const monitorPlayingVideo = function(){
    //  if(!streamPlaying.isPlaying && parseFloat(streamPlaying.analyser.context.currentTime) > 0){
      if(streamVideoPlaying && streamVideoPlaying.ended){
          streamPlaying = null;
          locateNextSong();
      }
      else{
         setTimeout(function(){
            monitorPlayingVideo();
         },5000);
      }
    };

    const monitorPlaylist = function(){
      remCatBo.getAllCatalogPlayList((_res)=>{
          console.log(_res);
          add2PlayList(_res);
          setTimeout(function(){
              monitorPlaylist();
          },10000);
      });
    };

    //Public
    mainMod.barDet = ko.observable();
    mainMod.playList = ko.observableArray([]);
    mainMod.playingSong = ko.observable(null);
    mainMod.playingVideo = ko.observable(null);
    mainMod.init = function(){
        remCatBo.rescAndGetPendingListFromLastSess((_res)=>{
            console.log(_res);
            add2PlayList(_res);
            locateNextSong();
            monitorPlaylist();
            var barDet = window.sessionStorage.getItem("session_info");
            barDet = JSON.parse(barDet||"{}");
            mainMod.barDet(barDet.barInfo);
            $('#qrConnect2Bar').qrcode({render: 'canvas',
                                        background: "#ffffff",
                                        size: 100,
                                        text: JSON.stringify({barId:mainMod.barDet().id,dayToken:mainMod.barDet().dayToken})});
            initMainListener(); // Set Mousemove listener
        });
    };
}
ko.applyBindings(new playerMod());
