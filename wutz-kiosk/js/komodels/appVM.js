// Main viewmodel class
define(['knockout',
        'bootstrap',
        'i18n!nls/nls.app.js',
        'i18nTool',
        'gritter',
        'jquery.qrcode'],
  function(ko, bt,i18nAtt,atTool) {
    return function appVM() {
        var starter = require(__dirname+"/node/InitBO");
        var mainMod = this;
    //Private
        const {ipcRenderer} = require('electron');
        var credBo = require(__dirname+"/node/CreditsBO");
        var albums = {};
        var songs = {};
        var loadedModule = "";
        var savedScrollPos = {};
        var jQueryInit = function(_locConfig){
             $('#qrConnect2Bar').qrcode({render: 'canvas',
                                         background: "#ffffff",
                                         size: 100,
                                         text: JSON.stringify({barId:mainMod.barDet().id,dayToken:mainMod.barDet().dayToken})});

            if(_locConfig.kioskMode){
                $("body").css("cursor","none");
                $.fn.disableSelection = function() {
                    return this
                            .attr('unselectable', 'on')
                            .css('user-select', 'none')
                            .on('selectstart', false);
                };
            }
            else{
                $("body").css("cursor","auto");
            }            

            /**
            $(".contentSection").kinetic({
                                              filterTarget: function(target, e){
                                                  if (!/down|start/.test(e.type)){
                                                      return !(/area|a|input/i.test(target.tagName));
                                                  }
                                              }
                                          });
                                          **/

        };

        var hideAllModules = function(_flowto, callback){
          console.log(koMods);
           var toHideArr = [];
           for(var key in koMods){
             if(key !== "app" && koMods[key].visible()){
                    toHideArr.push(key);
                    koMods[key].flowClass("animated fadeOut"+_flowto+"Big");
              }
           }
           _flowto === "Right"?"Left":"Right";
           setTimeout(function(){
             for(var i in toHideArr){
                 koMods[toHideArr[i]].visible(false);
                 koMods[toHideArr[i]].flowClass("animated fadeIn"+_flowto+"Big");
             }
             callback();
           },300);
        };
        var listObj = {interObj:null,countDown:10};
        const initMainListener = function(){
            $("body").click(function(){
                if(listObj.interObj)
                    clearInterval(listObj.interObj);
                listObj.countDown = 10;
                listObj.interObj = setInterval(function(){
                  if(listObj.countDown === 0){
                      const {ipcRenderer} = require('electron');
                      ipcRenderer.send('showFocusWin', {winId:"playerWin"});
                      clearInterval(listObj.interObj);
                  }
                  listObj.countDown = listObj.countDown-1;
                },1000);
            });
        };

        var initApplication = function(callback){
          var initComp = mainMod.initComponents();

          starter.init(function(_msg){
               if(_msg.err && _msg.err === "no_wutz"){
                  mainMod.messageBox("NO HAY CATALOGO CARGADO!","Tienes que cargar un catálogo para WutzKiosk. Cerrando aplicación en 6 segundos");
                  setTimeout(function(){
                    const remote = require('electron').remote;
                    let w = remote.getCurrentWindow();
                    w.close();
                  },6000);
               }
               else if(_msg.err && _msg.err === "no_login"){
                  mainMod.toLogIn({barid:_msg.barid,pass:""});
               }
               else{
                  mainMod.barDet(_msg.bar);
                  initComp = {loading:true,percAdv:10,msg:"Logueado a "+_msg.barid};
                  mainMod.initComponents(initComp);
                  
                  starter.indexCatalog(function(_update){
                      if(!_update.finished){
                         if(_update.arts)
                            mainMod.artists(_update.arts);

                         initComp = {loading:true,percAdv:_update.perc,msg:"Cargando Catálogo"};
                         mainMod.initComponents(initComp);
                      }
                      else{
                        initComp = {loading:false,percAdv:_update.perc,msg:"Carga Terminada"};
                        mainMod.initComponents(initComp);
                        koMods["artists"].artList(_update.arts);
                        koMods["albums"].allAlbums(_update.albs);
                        koMods["songs"].allSongs(_update.sngs);                        
                        starter.loadKioskConfigFile((_locConfig)=>{
                            mainMod.loading(false);
                            mainMod.kioskMode(_locConfig.kioskMode);
                            if(!mainMod.kioskMode()){
                                const remote = require('electron').remote;
                                let w = remote.getCurrentWindow();
                                w.setKiosk(false);
                            }
                            callback(_locConfig);
                        });
                        
                      }
                    //  callback(); //On FInish
                  });
               }
          });
        };

        var refreshCredit = function(callback){
                credBo.refreshCredit(function(_res){
                    console.log(_res);
                    if(_res.OK){
                       mainMod.credits(_res.totalCredits);
                       if(callback)
                          callback(mainMod.credits());
                    }
                });
        };

        var openPlayerWindow = function(){
          console.log("Opening Window");
          var _allParams = {
            session_info : JSON.parse(window.sessionStorage.getItem("session_info")),
            session_artists : JSON.parse(window.sessionStorage.getItem("session_artists")),
            session_albums : JSON.parse(window.sessionStorage.getItem("session_albums")),
            session_songs : JSON.parse(window.sessionStorage.getItem("session_songs"))
          };
          const pars = {name:"playerWin", contPath:"/templates/extTlp/player.html", options:{width:1200,height:800}};
          ipcRenderer.send('openPopUp', pars);
          ipcRenderer.send('sendMsg2Win', {send2:"playerWin",sessToken:"",value:_allParams});
        };

        const refreshSessionInterval = function(){
              var sessInfo = JSON.parse(window.sessionStorage.getItem("session_info"));
              console.log(sessInfo);
              starter.refresh(sessInfo.token, (_res)=>{
                console.log(_res);
                 if(_res.OK){
                      sessInfo.token = _res.token;
                      window.sessionStorage.setItem("session_info",JSON.stringify(sessInfo));
                      var _allParams = {refreshSessInfo:true,
                                        sessVar:{
                                              session_info : sessInfo
                                        }};
                      ipcRenderer.send('sendMsg2Win', {send2:"playerWin",sessToken:"",value:_allParams});
                 }
                 setTimeout(refreshSessionInterval, 120000);
              });
        };

    //Public
        mainMod.loading = ko.observable(false);
        mainMod.initComponents = ko.observable({loading:true,percAdv:0,msg:"Logueando"});
        mainMod.toLogIn = ko.observable({barid:"",pass:""});
        mainMod.barDet = ko.observable();
        mainMod.credits = ko.observable(0);
        mainMod.artists = ko.observableArray([]);
        mainMod.songsInQueue = ko.observableArray([]);
        mainMod.kioskMode = ko.observable(true);
        mainMod.locale = function(attrName, keys){
           return atTool.translateAtt(attrName, i18nAtt[attrName], keys);
        };

        mainMod.koTlpCont = ko.observable();
        mainMod.onkoModAppear = function(callback){
              console.log("app onLoad");
              mainMod.loading(true);
              loadedModule = "artists";
              initApplication(function(_locConfig){
                 console.log("done");
                 refreshCredit();
                 jQueryInit(_locConfig);
                 mainMod.getCurrentPlayList();
                 if(_locConfig.openPlayer)
                        openPlayerWindow();
                 initMainListener();
                 setTimeout(()=>{
                     refreshSessionInterval();
                 },120000);

              });
              if(callback){
                 callback();
              }
        };

        mainMod.userLogin = function(data){
            starter.login(data, function(_res){
                if(_res.logged){
                    location.reload();
                }
                else{
                   alert("Password Incorrecto! ... Intente de nuevo");
                }
            });
        };

        mainMod.loadMainModule = function(mainModName, _flowto){
            savedScrollPos[loadedModule] = $( "#"+loadedModule+"_cont" ).scrollTop();
            var flowto = _flowto||"Right";
            hideAllModules(flowto === "Right"?"Left":"Right", function(){
              koMods[mainModName].visible(true);
              koMods[mainModName].flowClass("animated fadeIn"+flowto+"Big");
              koMods[mainModName].onkoModAppear();
              loadedModule = mainModName;
              setTimeout(function(){
                  $( "#"+loadedModule+"_cont" ).scrollTop(savedScrollPos[mainModName]||0);
              },250);
            });
        };

        mainMod.messageBox = function(_title, _msg, _type){
              var type = _type||"default";
              var msgHtml = "<span class=\"label label-"+type+"\">"+_msg+"</span>"
              $.gritter.add({
                      title: _title,
                      text: msgHtml,
                      time: 4000
                  });

        };

        mainMod.transferCredits = function(){
              var currCredits = mainMod.credits();
                  credBo.getTransferCode(currCredits, (_res)=>{
                      console.log(_res);
                      if(_res.OK){
                          $('#qrTransferCred').html("");
                          var loadedTransCode = {barId: mainMod.barDet().id,dayToken:mainMod.barDet().dayToken,transferCode:_res.transfCode};
                          console.log(JSON.stringify(loadedTransCode));
                          $('#qrTransferCred').qrcode({render: 'canvas',
                                                      background: "#ffffff",
                                                      size: 400,
                                                      text: JSON.stringify(loadedTransCode)});
                        $("#transferCreditsMod").appendTo("body").modal();
                        return ;
                      }
                      mainMod.messageBox("No QR", "ha ocurrido un error generando código QR. Favor intentelo más tarde","danger");
                  });
        };

        mainMod.checkColectedCredits = function(){
            var currCre = mainMod.credits();
            refreshCredit((_cred)=>{
                if(currCre !== _cred) { //Not Colected Yet
                  mainMod.messageBox("Créditos Cajeados", "Los créditos fueron canjeados exitosamente","primary");
                }
                $("#transferCreditsMod").modal("hide");
            });
        };

        mainMod.getCurrentPlayList = function(){
            koMods.songs.getCurrentPlayList(5,(_res)=>{
                mainMod.songsInQueue(_res);
                setTimeout(function(){
                    mainMod.getCurrentPlayList();
                },10000);
            });
            /**
           var inter = setInterval(function(){
             koMods.songs.getCurrentPlayList(5,(_res)=>{
                 mainMod.songsInQueue(_res);
                 clearInterval(inter);
             });
           },7000);
           **/
        };

        mainMod.showHideSearchMenu = function(){
          var filterClass = $(".filterSection").attr("class");
          var contClass = $($(".contentSection")[$(".contentSection").length-1]).attr("class");
          if(/col-xs-0/.test(filterClass)){ //Means search menu closed
               $("#filterButton i").removeClass("fa-search").addClass("fa-times");
               $("#filterButton").addClass("open");
               $(".filterTable").show();
               filterClass = filterClass.replace("col-xs-0","col-xs-2");
               contClass = contClass.replace(/col-xs-[0-9]{1,2}/ig, function(itm){
                                                                      return "col-xs-"+(parseInt(itm.split("-")[2])-2);
                                                                   });
          }
          else{
            $("#filterButton i").removeClass("fa-times").addClass("fa-search");
            $("#filterButton").removeClass("open");
            $(".filterTable").hide();
            filterClass = filterClass.replace("col-xs-2","col-xs-0");
            contClass = contClass.replace(/col-xs-[0-9]{1,2}/ig, function(itm){
                                                                   return "col-xs-"+(parseInt(itm.split("-")[2])+2);
                                                                });
          }
          $(".filterSection").attr("class", filterClass);
          $($(".contentSection")[$(".contentSection").length-1]).attr("class",contClass);
        };

        mainMod.keyListener = function(data, ev){
            console.log(ev.key);
            if(ev.key === "c"){
                credBo.addCredit((_res)=>{
                    console.log(_res);
                    if(_res.OK)
                       mainMod.credits(_res.totalCredits);
                });
            }
            else if(ev.key === "t"){
                mainMod.transferCredits();
            }
        };
    };
});
