var dom = __dirname;
var koMods = {};
window.$ = window.jQuery = require('./js/jquery-1.10.2');
window.$.kinetic = require('jquery.kinetic');
require.nodeRequire = require;
requirejs.config({
  //    config: {
    //          i18n: {
      //            locale: 'es'
        //      }
      //},
      shim: {
          'bootstrap': {
              deps: ['jquery']
          },
          'knockout': {
              deps: ['jquery']
          },
          'jquery.metisMenu': {
              deps: ['jquery']
          },
          'gritter': {
              deps: ['jquery']
          },
          'jquery.slimscroll': {
              deps: ['jquery']
          },
          'jquery.peity': {
              deps: ['jquery']
          },
          'masonry.pkgd': {
              deps: ['jquery']
          },
          'inspinia': {
              deps: ['jquery','jquery.metisMenu']
          },
          'pace': {
              deps: ['jquery']
          },
          'icheck': {
              deps: ['jquery']
          },
          'bootstrap2-toggle': {
                deps: ['jquery', 'bootstrap']
          },
          'jquery.qrcode': {
                 deps: ['jquery']
          }
      },
      paths: {
          'jquery': dom+'/js/jquery-1.10.2',
          'knockout': dom+'/js/knockout-3.4.2',
          'bootstrap': dom+'/js/bootstrap.min',
          'jquery.metisMenu': dom+'/js/plugins/metisMenu/jquery.metisMenu',
          'jquery.slimscroll': dom+'/js/plugins/slimscroll/jquery.slimscroll.min',
          'jquery.peity': dom+'/js/plugins/peity/jquery.peity.min',
          'masonry.pkgd': dom+'/js/masonry.pkgd.min',
          'inspinia': dom+'/js/inspinia',
          'pace': dom+'/js/plugins/pace/pace.min',
          'icheck': dom+'/js/plugins/iCheck/icheck.min',
          'i18n': dom+'/js/i18n',
          'i18nTool': dom+'/js/lib/i18nTool',
          'arrTool': dom+'/js/lib/arrTools',
          'text': dom+'/js/text',
          'bootstrap2-toggle': dom+'/js/plugins/bootstrap2-toggle/bootstrap2-toggle.min',
          'gritter': dom+'/js/plugins/gritter/jquery.gritter.min',
          'jquery.qrcode': dom+'/js/plugins/jquery-qrcode/jquery-qrcode-0.14.0.min',
          'keyListener': dom+'/js/lib/keyListener'
      }
});
requirejs(['knockout',
         'js/komodels/appVM.js',
         'js/komodels/artistsVM.js',
         'js/komodels/albumsVM.js',
         'js/komodels/songsVM.js'],
  function(ko,
           appMod,
           artistsMod,
           albumsMod,
           songsMod)  // Load All Extra Modules
  {
        koMods["app"] = new appMod();

        ko.applyBindings(koMods["app"]);
        koMods["app"].onkoModAppear(function(){
          //Loading Modules Zone
          koMods["artists"] = new artistsMod();
          var tlp = $(koMods["artists"].template);
          $("koTlpCont").append(tlp);
          ko.applyBindings(koMods["artists"], tlp[0]);
          //-------------------------------------------------------------
          koMods["albums"] = new albumsMod();
          var tlp = $(koMods["albums"].template);
          $("koTlpCont").append(tlp);
          ko.applyBindings(koMods["albums"], tlp[0]);
          //-------------------------------------------------------------
          koMods["songs"] = new songsMod();
          var tlp = $(koMods["songs"].template);
          $("koTlpCont").append(tlp);
          ko.applyBindings(koMods["songs"], tlp[0]);
          //-------------------------------------------------------------

          // /End Seting Modules
          //Set Main Module as Init Module
          koMods["artists"].onkoModAppear();
          koMods["artists"].visible(true);
        });

   }
);
