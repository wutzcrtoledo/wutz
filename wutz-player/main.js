const electron = require('electron');
var app = electron.app;  // Module to control application life.
var BrowserWindow = electron.BrowserWindow;  // Module to create native browser window.

const ipcMain = require('electron').ipcMain;


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;

//var playerWindow = null;
// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform != 'darwin') {
    app.quit();
  }


});

ipcMain.on('setFullScreen', function(event, arg) {
  //event.returnValue = app.getAppPath();
   mainWindow.setFullScreen(true);
});
ipcMain.on('exitFullScreen', function(event, arg) {
  //event.returnValue = app.getAppPath();
   mainWindow.setFullScreen(false);
});

ipcMain.on('toogFullScreen', function(event, arg) {
  //event.returnValue = app.getAppPath();
   if (mainWindow)
           mainWindow.setFullScreen(!mainWindow.isFullScreen());

   event.returnValue = true;
});


ipcMain.on('justClose', function(event, arg) {
  //event.returnValue = app.getAppPath();

    if (mainWindow)
           mainWindow.close();

    event.returnValue = true;
});

ipcMain.on('minimize', function(event, arg) {
  //event.returnValue = app.getAppPath();
    if (mainWindow)
           mainWindow.minimize();
    event.returnValue = true;
});



ipcMain.on('getAppPath', function(event, arg) {
  event.returnValue = app.getAppPath();
});

ipcMain.on('isDevMode', function(event, arg) {
  event.returnValue = true; //Switch to stablish dev or release version
});


//logger.setLocalItem("appPath",app.getAppPath());
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 1300,
                                  height: 625,
                                  frame: true ,
                                  titleBarStyle: 'customButtonsOnHover',
                                  transparent: false,
                                  'web-preferences': {'web-security': false}});

  // and load the index.html of the app.
  //mainWindow.loadURL('file://' + __dirname + '/index.html');
  mainWindow.loadURL('file://' + __dirname + '/login_reg.html');
  // mainWindow.loadURL('file://' + __dirname + '/test.html');
  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
});

//Function to manage PopUps
function createPopUp (name, contPath, options, callback) {
  if(typeof options === "function"){
     callback = options;
     options = null;
  }
  options = options||{width: 600, height: 1300};

  // Create the browser window.

    popUps[name] = new BrowserWindow(options);
    popUps[name].loadURL('file://' + __dirname + contPath);
    popUps[name].on('closed', function () {
      popUps[name] = null;
    });

  if(callback)
     callback(popUps[name]);
};

ipcMain.on('openPopUp', (event, arg) => {
  //event.returnValue = app.getAppPath();
     if(!popUps || !popUps[arg.name]){
          console.log("---------------------------------");
          console.log(arg);
           createPopUp (arg.name, arg.contPath, arg.options, (_win)=>{
               event.sender.send('whereIsMyPopUp', _win);
           });
     }
     else if(popUps){
        popUps[arg.name].focus();
     }
});

ipcMain.on('sendMsg2Win', (event, arg) => {
  //event.returnValue = app.getAppPath();
  var name = arg.send2;
  var inter = setInterval(function(){
      if(popUps[name].ready){
         clearInterval(inter);
         popUps[name].webContents.send('win-request', arg);
      }
  },200);
});

ipcMain.on('markWinReady', (event, arg) => {
  //event.returnValue = app.getAppPath();
  var name = arg.winName;
  popUps[name]["ready"] = true;
});
