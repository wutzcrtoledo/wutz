//var ncp = require('ncp').ncp;
var uglifyJS = require("uglify-js");
var fs = require("fs-extra");
var path = require('path');


var walkJS = function(dir, fileExt, done) {
  var results = [];
  fs.readdir(dir, function(err, list) {
    if (err) return done(err);
    var pending = list.length;
    if (!pending) return done(null, results);
    list.forEach(function(file) {
      file = path.resolve(dir, file);
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walkJS(file, fileExt,function(err, res) {
            results = results.concat(res);
            if (!--pending) done(null, results);
          });
        }
        else {
          if(file.toLowerCase().indexOf(fileExt) !== -1) {
                results.push(file);
          }
          if (!--pending) done(null, results);
        }
      });
    });
  });
};

var config = {distFolder: "../dist/WutzApp-dist",
              contFolder: "./www",
              css2Minify: ["wutzapp.css"]};


var minifyAndSaveFile = function(_path, callback){
  var result = uglifyJS.minify(_path);
  //console.log(result);
  console.log("Minifying ... "+_path);
  fs.writeFile(_path, result.code,function(err){
      if(err){
          console.log(JSON.stringify({"error":err}));
          if(callback)
                callback();
      }
  });
};

var minifyFiles = function(){
     minifyJsFiles();
     //minifyCSSFiles();
};

var minifyJsFiles = function(){
  walkJS(config.distFolder+"/js/komodels",".js",function(err, _jsFiles){
       console.log(_jsFiles);
       for(var i in _jsFiles){
          var _val = _jsFiles[i];
          minifyAndSaveFile(_jsFiles[i]);
       }
  });
  walkJS(config.distFolder+"/js/lib",".js",function(err, _jsFiles){
       console.log(_jsFiles);
       for(var i in _jsFiles){
          var _val = _jsFiles[i];
          minifyAndSaveFile(_jsFiles[i]);
       }
  });
};


var minifyCSSFiles = function(){
  for(var i in config.css2Minify){
      minifyAndSaveFile(config.distFolder+"/css/"+config.css2Minify[i]);
  }
};

var copyCounter = 0;


fs.copy(config.contFolder, config.distFolder, err => {
  if (err) return console.error(err)
  console.log('success!');
  copyCounter++;
  //setTimeout(function(){
    // minifyFiles();
  //},10000);
});

/**
ncp(config.contFolder, config.distFolder, function (err) {
 if (err) {
   return console.error(err);
 }
 copyCounter++;
 minifyJsFiles();
});
**/

fs.copy("config.xml", config.distFolder+"/config.xml", err => {
  if (err) return console.error(err)
  console.log('success!');
  fs.readFile(config.distFolder+"/config.xml", 'utf8', function (err,data) {
       if (err) {
         return console.log(err);
       }
       var result = data.replace(/www\//ig, '');
       fs.writeFile(config.distFolder+"/config.xml", result, 'utf8', function (err) {
          if (err) return console.log(err);
          copyCounter++;
       });
  });
});

/**
ncp("config.xml", config.distFolder+"/config.xml", function (err) {
 if (err) {
   return console.error(err);
 }
 fs.readFile(config.distFolder+"/config.xml", 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      var result = data.replace(/www\//ig, '');
      fs.writeFile(config.distFolder+"/config.xml", result, 'utf8', function (err) {
         if (err) return console.log(err);
         copyCounter++;
      });
 });
});
**/

fs.copy("README.md", config.distFolder+"/README.md", err => {
  if (err) return console.error(err)
  console.log('success!');
  copyCounter++;

});
/**
ncp("README.md", config.distFolder+"/README.md", function (err) {
 if (err) {
   return console.error(err);
 }
 copyCounter++;
});
**/

/**
var inter = setInterval(function(){
      if(copyCounter >= 2){
        clearInterval(inter);
        console.log("Done");
      }
},100);
**/
