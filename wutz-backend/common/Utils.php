<?php
//include_once $_SESSION["ROOT_PATH"].'/serv/common/getID3/getid3/getid3.php';
//require_once('../getid3/getid3.php');

class Utils 
{
     private $songsArray;

         public function Utils()
         {
             $this->songsArray = array();
         }

	public function generateHTMLRows($tableArray)
	{
		$table="";
		foreach ($tableArray as $row)
		{
		  $table.="<tr>";
			foreach ($row as $value)
			{
			 $table.= "<td>$value</td>";
			}
		  $table.="</tr>";
		}
		return $table;
	}


	public function getXMLFromRSArray($arrayRS,$rootNodeName = "ROWSET", $nodeName = "ROW")
        {
            $resXml = "";
             $resXml.= "<".strtoupper($rootNodeName).">\n";
                foreach ($arrayRS as $key => $value)
                {
                   $resXml.=  "<".strtoupper($nodeName).">\n";
                   foreach($value as $key2 => $value2)
                   {
                        $resXml.=  "  <".strtoupper($key2)."><![CDATA[".$value2."]]></".strtoupper($key2).">\n";
                   }
                   $resXml.=  "</".strtoupper($nodeName).">\n";
                }
             $resXml.=  "</".strtoupper($rootNodeName).">\n";

             return $resXml;
        }

        public function getXMLFromArray($arrObj, $mainNodeName = "XMLOBJ")
        {
            $resXml = "";
                foreach ($arrObj as $key => $value)
                {
                   $tagName = "";
                   if(is_int($key))
                   {
                       if(isset($arrObj["_IDXNAME_"]))
                           $tagName = $arrObj["_IDXNAME_"];
                       else
                           $tagName = $mainNodeName."_ROW";
                   }
                   else
                       $tagName = $key;

                   if($key !== "_IDXNAME_")
                   {
                        $resXml.=  "<".strtoupper($tagName).">";
                        if(is_array($value))
                        {
                           $resXml.= "\n".$this->getXMLFromArray($value, $tagName);
                        }
                        else
                        {
                            $resXml.=  "<![CDATA[".$value."]]>";
                        }
                        $resXml.=  "</".strtoupper($tagName).">\n";
                   }
                }
             return $resXml;

        }

        private function loopArrayAndGetXML($arrObj)
        {
            /*
            $resXml = "<".strtoupper($mainNodeName).">";
            $resXml .= $this->loopArrayAndGetXML($arrObj);
            $resXml .= "</".strtoupper($mainNodeName).">";
            return $resXml;
             *
             */
            $resXml = "";
                foreach ($arrObj as $key => $value)
                {
                   $resXml.=  "<".strtoupper($key).">";
                   if(is_array($value))
                   {
                      $resXml.= "\n".$this->loopArrayAndGetXML($value);
                   }
                   else
                   {
                       $resXml.=  "<![CDATA[".$value."]]>";
                   }
                   $resXml.=  "</".strtoupper($key).">\n";
                }
             return $resXml;
        }


        function getBase64ImgFromFile($filePath){

            if(isset($filePath) && $filePath!==""){
               try{
                  // print($filePath."</br>");
                    $type = pathinfo($filePath, PATHINFO_EXTENSION);
                    $data = file_get_contents($filePath);
                    $base64Img = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    return $base64Img;
               }
               catch (Exception $e)  {
 //                  print($filePath."</br>");
               //    print_r($e);
                   return "";
               }
            }
            else{
                return "";
            }
        }

        function base64_to_jpeg( $base64_string, $output_file ) {
             $ifp = fopen($output_file, "wb");
             $data = explode(',', $base64_string);
             if(sizeof($data) > 1)
                fwrite($ifp, base64_decode($data[1]));
             else
                fwrite($ifp, base64_decode($base64_string));
             fclose($ifp);
            return $output_file;
        }

        function getHTMLArray($dat){
          if (is_string($dat)) return htmlspecialchars($dat,ENT_HTML5 | ENT_SUBSTITUTE, 'UTF-8');
          if (!is_array($dat)) return $dat;
          $ret = array();
          foreach($dat as $i=>$d) $ret[$i] = $this->getHTMLArray($d);
          return $ret;
        }

        function utf8_encode_all($dat){ // -- It returns $dat encoded to UTF8
          if (is_string($dat)) return utf8_encode($dat);
          if (!is_array($dat)) return $dat;
          $ret = array();
          foreach($dat as $i=>$d) $ret[$i] = $this->utf8_encode_all($d);
          return $ret;
        }

        function sendEmail($to,$subject,$message){
            if(sizeof($to) > 1){
                $to = implode(",", $to);
            }
            mail($to, $subject, $message);
        }

        public function implodeArrForSQL($arr){
            $implodedList = implode("||", $arr);
            $implodedList = preg_replace('/[\']+/i', "\\'", $implodedList);
            $implodedList = preg_replace('/[^||]+/i', "'$0'", $implodedList);
            $implodedList = preg_replace('/[||]+/i', ",", $implodedList);
            return $implodedList;
        }

        public function diffFilterArray($filterColName,$arr1,$arr2Filter){
            foreach ($arr1 as $idx => $nod) {
                $nodeValue2Compare = isset($nod[$filterColName])?$nod[$filterColName]:$nod;
                $nodeValue2Compare = trim(str_replace(" ", "", $nodeValue2Compare));
                foreach ($arr2Filter as $key => $arr2FilterNode) {
                    if($nodeValue2Compare === trim(str_replace(" ", "", $arr2FilterNode))){
                        unset($arr1[$idx]);
                    }
                }
            }
            return $arr1;
        }
}
