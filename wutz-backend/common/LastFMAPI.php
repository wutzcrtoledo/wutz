<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LastFMAPI
 *
 * @author CRTOLEDO
 */
class LastFMAPI {
    private $config;
    
    public function LastFMAPI(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }
    
    public function searchArtist($artistName){
        $artistName = preg_replace('/\([0-9]+\)/i', "", $artistName); // Removind Dates On Parentesis
        $artistName = preg_replace('/[\s\s]+/i'," ", trim($artistName));
        $artistName = urlencode($artistName);
        $parms = "artist.getinfo&artist=".$artistName."&autocorrect=1";
        print("Search [".$parms."]\n"); 
        return json_decode($this->restCall($parms, "GET"), true);
    }
    public function searchAlbum($artistName, $albumName){
        $artistName = preg_replace('/\([0-9]+\)/i', "", $artistName); // Removind Dates On Parentesis
        $artistName = preg_replace('/[\s\s]+/i'," ", trim($artistName));
        $artistName = urlencode($artistName);
         $albumName = preg_replace('/\([0-9]+\)/i', "", $albumName); // Removind Dates On Parentesis
        $albumName = preg_replace('/[\s\s]+/i'," ", trim($albumName));
        $albumName = urlencode($albumName);
        $parms = "album.getinfo&artist=".$artistName."&album=".$albumName."&autocorrect=1";
        print("Search [".$parms."]\n"); 
        return json_decode($this->restCall($parms, "GET"),true);
    }
    
    public function searchSong($artistName,$songName){
        $artistName = urlencode($artistName);
        $songName = urlencode($songName);
        $parms = "track.getInfo&artist=".$artistName."&track=".$songName."&autocorrect=1";
        return json_decode($this->restCall($parms, "GET"), true);
    }
    
    private function restCall($wsToCall,$method){
        $servPath = $this->config["lastFMUrl"];
        $wsToCall = "method=".$wsToCall .= "&api_key=".$this->config["lastFMToken"]."&format=json";
        $opts = array(
          'http'=>array(
            'method'=>$method,
            'header'=>"Content-type: application/json; "
                                 . " charset=utf-8\r\n; "
                                 . " User-Agent:MyAgent/1.0\r\n",
            'content'=>''
          )
        );
        $context = stream_context_create($opts);
        $jsonRes = file_get_contents($servPath.$wsToCall, false, $context);
        return $jsonRes;
    }
      
}
