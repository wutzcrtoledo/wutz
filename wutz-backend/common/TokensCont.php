<?php
/**
 * Description of WutzAdminBO
 *
 * @author CRTOLEDO
 */
class TokensCont {
    private static $tokens;
    private function TokensCont(){
    }
    public static function setToken($key, $token){
       if(!isset($_SESSION["sesTokens"])){
          TokensCont::$tokens = array();
       }
       else {
         TokensCont::$tokens = $_SESSION["sesTokens"];
       }
       if(!isset(TokensCont::$tokens[$key])){
          TokensCont::$tokens[$key] = array();
       }
       TokensCont::$tokens[$key] = $token;
       $_SESSION["sesTokens"] = TokensCont::$tokens;
    }
    public static function getToken($key){
       TokensCont::$tokens = isset($_SESSION["sesTokens"])?$_SESSION["sesTokens"]:null;
       if(!isset(TokensCont::$tokens)){
          return false;
       }
       if(!isset(TokensCont::$tokens[$key])){
          return false;
       }
       return TokensCont::$tokens[$key];
    }
    public static function removeToken($key){
      TokensCont::$tokens = $_SESSION["sesTokens"];
       if(!isset(TokensCont::$tokens)){
          return true;
       }
       if(!isset(TokensCont::$tokens[$key])){
          return true;
       }
       unset(TokensCont::$tokens[$key]);
       if(!isset(TokensCont::$tokens[$key])){
          return true;
       }
       $_SESSION["sesTokens"] = TokensCont::$tokens;
       return false;
    }
    public static function checkAuthorization($key,$token){
        if(!isset($token) || $token == "" || !isset($key) || $key == "")
           return false;
        $currTkn = TokensCont::getToken($key);
        if($currTkn == $token)
            return true;
        else {
            return false;
        }
    }
}
