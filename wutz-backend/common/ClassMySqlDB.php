<?php

class ClassMySqlDB{
	private $baseName='db_wutz';
	private $host='localhost';
	//private $userName='wutzAdminUser';
	//private $psw='mARnW7w7eRdZGm7p';
  private $userName='bz_apps';
	private $psw='FPZ8msSRvI6T2B0D';

	private $conn;
//	private final static $connBack;

//----------------------------------------------------------------------------------------
   public function ClassMySqlDB()
   {
       $this->connect();
   }
//----------------------------------------------------------------------------------------
  public function connect()
  {
      try
      {
          $this->conn = new mysqli($this->host, $this->userName, $this->psw, $this->baseName);
          $this->conn->set_charset("utf8");
          if ($this->conn->connect_errno) {
            echo "Fallo al conectar a MySQL: " . $this->conn->connect_errno;
          }
      }
      catch(Exception $e){
          print 'No Conectado .'.$e;
      }
  }
//----------------------------------------------------------------------------------------
  public function disconnect()
  {
  	$this->conn->close();

  }
//----------------------------------------------------------------------------------------
  public function getArrayFromQuery($query)
  {
           $this->connect();
           $resEmp = $this->conn->query($query);
           $totEmp = $resEmp->num_rows;
           $tempArray=array();
           $rsArray=array();
            if ($totEmp> 0)
            {
                while ($rowEmp = $resEmp->fetch_assoc())
                {
                        foreach ($rowEmp as $key => $value)
                        {
                                $tempArray[$key]=$value;
                        }
                        $rsArray[]=$tempArray;
                }
            }
            $resEmp->close();
            $this->disconnect();
            return $rsArray;
   }
//----------------------------------------------------------------------------------------

   public function executeTransaction($query)
  {
    $this->connect();
    $this->conn->autocommit(false);
    $afectedRows = 0;
    if (!$this->conn->query($query)) {
    		printf("Errormessage: %s\n", $this->conn->error);
     }
    $afectedRows = $this->conn->affected_rows;
     $this->conn->commit();
     $this->disconnect();
  	return $afectedRows;
  }

  public function escape_string($str){

       $val = $this->conn->real_escape_string($str);
      return $val;
  }
}
