<?php
        
class ClassMySqlDB{
	private $baseName='WutzAdmin';
	private $host='localhost';
	//private $userName='wutzAdminUser';
	//private $psw='mARnW7w7eRdZGm7p';
        private $userName='root';
	private $psw='';

	private $conn;
//	private final static $connBack;
	
//----------------------------------------------------------------------------------------	
   public function ClassMySqlDB()
   {
       $this->connect();
   }
//----------------------------------------------------------------------------------------	
  public function connect() 
  {
      try
      {
          $this->conn = new mysqli($this->host, $this->userName, $this->psw, $this->baseName);
          $this->conn->set_charset("utf8");
          if ($this->conn->connect_errno) {
           // echo "Fallo al conectar a MySQL: " . $this->conn->connect_errno;
          }
      }
      catch(Exception $e){
          //print 'No Conectado .'.$e;
      }       
  }
//----------------------------------------------------------------------------------------
  public function disconnect()
  {
  	$this->conn->close();
    
  }
//----------------------------------------------------------------------------------------
  public function getArrayFromQuery($query)
  {
     // print_r($query);
           $this->connect();
           $resEmp = $this->conn->query($query);
           if(isset($resEmp)){
                $totEmp = $resEmp->num_rows;
           }
           else{
               $totEmp = 0;
           }
           $tempArray=array();
           $rsArray=array();
            if ($totEmp> 0)
            {
                if($totEmp == 1){
                    $rsArray[0] = $resEmp->fetch_assoc();
                }
                while ($rowEmp = $resEmp->fetch_assoc())
                {
                        foreach ($rowEmp as $key => $value)
                        {
                                $tempArray[$key]=$value;
                        }
                        $rsArray[]=$tempArray;
                }
            }
            $resEmp->close();
            $this->disconnect();
            return $rsArray;
   }
//----------------------------------------------------------------------------------------

   public function executeTransaction($query)
  {
    $this->connect();
    $this->conn->autocommit(false);
    $afectedRows = 0; 
    if (!$this->conn->query($query)) {
    	//	printf("Errormessage: %s\n", $this->conn->error);
     }
    $afectedRows = $this->conn->affected_rows;
     $this->conn->commit();
     $this->disconnect();
  	return $afectedRows;
  }
  
  public function escape_string($str){
       
       $val = $this->conn->real_escape_string($str);
      return $val;
  }
}