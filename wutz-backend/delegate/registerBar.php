<?php
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';

header('Content-Type: application/json');
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: POST'); 

if($_SERVER['REQUEST_METHOD'] == "POST"){
    $wutzAdminBo = new WutzAdminBO();
    $json = file_get_contents('php://input'); 
    //print_r($json);
    $json = urldecode($json);
    $obj = json_decode($json, true);
    if($wutzAdminBo->registerBar($obj)){
        print(json_encode(array("OK"=>true)));
    }
    else {
        print(json_encode(array("OK"=>false,"msg"=>"No changes made or update failed")));
    }
   //print_r($obj);
    
}