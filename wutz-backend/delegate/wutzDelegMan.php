<?php
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/WutzCatalogBO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/PlayManagerBO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';
//session_sta
//rt();

//$request = $_REQUEST;
//$fnc = $request["fnc"];

class WutzDelegMan {
    public function dispatchRequest($heds,$fnc, $request) {
        header('Content-Type: application/json');
        if($fnc === "getBar"){
            $this->getBar($request);
        }
        if($fnc === "getNearByBars"){
            $this->getNearByBars($request);
        }
        if($fnc === "getBarDetails"){
            $this->getBarDetails($request);
        }
        else if($fnc === "getArtistList"){
            $this->getArtistList($request);
        }
        else if($fnc === "getAlbumPerArtist"){
            $this->getAlbumPerArtist($request);
        }
        else if($fnc === "getSongsPerAlbum"){
            $this->getSongsPerAlbum($request);
        }
        else if($fnc === "checkToken"){
            $this->checkToken($request);
        }
        else if($fnc === "addSongToQueue"){
            $this->addSongToQueue($request);
        }
        else if($fnc === "searchOnCatalog"){
            $this->searchOnCatalog($request);
        }
        else if($fnc === "getArtistSongs"){
            $this->getArtistSongs($request);
        }
        else if($fnc === "getPlayListSongs"){
            $this->getPlayListSongs($request);
        }
        else if($fnc === "connect2Bar"){
            $this->connect2Bar($request);
        }
        else if($fnc === "checkCredits"){
            $this->checkCredits($request);
        }
    }

    //---------------------------
    private function getBar($request){
        $managerBo = new WutzAdminBO();
        $barId = $request["barId"];
            $phpArr = $managerBo->searchBar($barId);
            $jsonFinRes = json_encode($phpArr);
            print $jsonFinRes;
    }
    
    private function getNearByBars($request){
        if($_SERVER['REQUEST_METHOD'] == "GET"){
                $managerBo = new WutzAdminBO();
                $usrLat  = $request["lat"];
                $usrLong = $request["lon"];
                $res = $managerBo->getNearByBars($usrLat, $usrLong);
                if(!$res){
                    print json_encode(array("error"=>"No List"));
                }
                else{
                    print json_encode($res);
                }
        }
        else{
            print json_encode(array("error"=>"Wrong Method"));
        }
    }
    
    private function getBarDetails($request){
       // header('Access-Control-Allow-Origin: *');
       // header('Access-Control-Allow-Methods: GET');
        if($_SERVER['REQUEST_METHOD'] == "GET"){
                $managerBo = new WutzAdminBO();
                $barId  = $request["barId"];
                $checkAvailable = isset($request["checkAvailable"]);
                   $res = $managerBo->getBarDetails($barId);
                    if(!$res){
                      if($checkAvailable){
                         print json_encode(array("available"=>true));
                      }
                      else{
                         print json_encode(array("error"=>"No List"));
                      }
                    }
                    else if(!$checkAvailable){
                        $catVerfile = $_SESSION["ROOT_PATH"]."/cache/version_cat_".$res["idcatalog"].".txt";
                        if(!file_exists($catVerfile)){
                            $res["catVersion"] = 0;
                        }
                        else{
                            $res["catVersion"] = file_get_contents($catVerfile);
                        }
                        print json_encode($res);
                    }
                    else{
                       print json_encode(array("available"=>false));
                    }
        }
        else{
            print json_encode(array("error"=>"Wrong Method"));
        }
    }
    
    private function getArtistList($request){
         if($_SERVER['REQUEST_METHOD'] == "GET"){
            $catId = $request["catId"];
    
            $cachefile = $_SESSION["ROOT_PATH"]."/cache/artists_".$catId.".json";
            if (file_exists($cachefile)){
               include($cachefile);
               exit;
            }
            ob_start();
            $managerBo = new WutzCatalogBO();
            print json_encode($managerBo->getAllArtist($catId));
    
             $fp = fopen($cachefile, 'w');
                  fwrite($fp, ob_get_contents());
                  fclose($fp);
             ob_end_flush();
        }
    }
    private function getAlbumPerArtist($request){
        if($_SERVER['REQUEST_METHOD'] == "GET"){
            $catId = $request["catId"];
    
            $cachefile = $_SESSION["ROOT_PATH"]."/cache/albums_".$catId."_".$request["artId"].".json";
            if (file_exists($cachefile)){
               include($cachefile);
               exit;
            }
            ob_start();
    
            $managerBo = new WutzCatalogBO();
            print json_encode($managerBo->getAlbumPerArtist($catId,$request["artId"]));
    
             $fp = fopen($cachefile, 'w');
                  fwrite($fp, ob_get_contents());
                  fclose($fp);
             ob_end_flush();
    
    
        }
    }
    private function getSongsPerAlbum($request){
        if($_SERVER['REQUEST_METHOD'] == "GET"){
            $catId = $request["catId"];
    
            $cachefile = $_SESSION["ROOT_PATH"]."/cache/songs_".$catId."_".$request["albId"].".json";
            if (file_exists($cachefile)){
               include($cachefile);
               exit;
            }
            ob_start();
    
            $managerBo = new WutzCatalogBO();
            print json_encode($managerBo->getAlbumSongs($catId, $request["albId"]));
    
             $fp = fopen($cachefile, 'w');
                  fwrite($fp, ob_get_contents());
                  fclose($fp);
             ob_end_flush();
        }
    }
    
    private function searchOnCatalog($request){
        if($_SERVER['REQUEST_METHOD'] == "GET"){
            $catId = $request["catId"];
            $keyTxt = $request["keyTxt"];
    
            $cachefile = $_SESSION["ROOT_PATH"]."/cache/search_".$catId."_".$keyTxt.".json";
            if (file_exists($cachefile)){
               include($cachefile);
               exit;
            }
            ob_start();
    
            $managerBo = new WutzCatalogBO();
            print json_encode($managerBo->searchInCatalog($catId, $keyTxt));
    
             $fp = fopen($cachefile, 'w');
                  fwrite($fp, ob_get_contents());
                  fclose($fp);
             ob_end_flush();
        }
    }
    
    private function getArtistSongs($request){
        if($_SERVER['REQUEST_METHOD'] == "GET"){
            $catId = $request["catId"];
            $artId = $request["artId"];
    
            $cachefile = $_SESSION["ROOT_PATH"]."/cache/songs_".$catId."_".$artId.".json";
            if (file_exists($cachefile)){
               include($cachefile);
               exit;
            }
            ob_start();
    
            $managerBo = new WutzCatalogBO();
            print json_encode($managerBo->getArtistSongs($catId, $artId));
    
             $fp = fopen($cachefile, 'w');
                  fwrite($fp, ob_get_contents());
                  fclose($fp);
             ob_end_flush();
        }
    }
    
    private function checkToken($request){
        if($_SERVER['REQUEST_METHOD'] == "POST"){
        //        "{"catId":"2","token":"123123"}
            $json = file_get_contents('php://input');
            $obj = json_decode($json, true);
            $catId = $obj["catId"];
            $token = $obj["token"];
            $managerBo = new WutzAdminBO();
            $res = array("tokenOK"=>$managerBo->isThisDayToken($catId, $token));
            print json_encode($res);
        }
    }
    
    private function addSongToQueue($request){
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                $json = file_get_contents('php://input');
                $obj = json_decode($json, true);
                $catId = $obj["catId"];
                $token = $obj["token"];
                $songId = $obj["songId"];
                $guid = $obj["guid"];
               // $ip = $_SERVER['REMOTE_ADDR'];
                $managerBo = new PlayManagerBO();
                $res = $managerBo->addSongToPlayList($catId, $token, $guid, $songId);
                print json_encode($res);
            }
    }
    
    private function getPlayListSongs($request){
      if($_SERVER['REQUEST_METHOD'] == "POST"){
          $json = file_get_contents('php://input');
          $obj = json_decode($json, true);
          $catId = $obj["catId"];
          $token = $obj["token"];
          $ip = $_SERVER['REMOTE_ADDR'];
          $managerBo = new PlayManagerBO();
          $res = $managerBo->getPlayListSongs($token, $catId, true);
          print json_encode($res);
      }
    }
    
    private function connect2Bar($request){
      if($_SERVER['REQUEST_METHOD'] == "POST"){
          $json = file_get_contents('php://input');
          $obj = json_decode($json, true);
          $catId = $obj["catId"];
          $dayToken = $obj["token"];
          $guid  =  $obj["guid"];
          $managerBo = new WutzAdminBO();
          $res = $managerBo->connect2Bar($catId,$guid,$dayToken);
          print json_encode($res);
      }
    }
    
    private function checkCredits($request){
      if($_SERVER['REQUEST_METHOD'] == "POST"){
          $json = file_get_contents('php://input');
          $obj = json_decode($json, true);
          $catId = $obj["catId"];
          $guid = $obj["guid"];
          $credits = 0;
          $managerBo = new WutzAdminBO();
          $res = $managerBo->addCreditsTo($guid, $catId, $credits);
          print json_encode($res);
      }
    }
}