<?php

include_once $_SESSION["ROOT_PATH"].'/bo/GenCatalogBO.php';
include_once $_SESSION["ROOT_PATH"].'/common/TokensCont.php';
$heds = getallheaders();
$authToken = isset($heds["Authorization"])?$heds["Authorization"]:"";
header('Content-Type: application/json');
if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
   print('{"error":{"code":1,"msg":"Not Authorized"}}');
    exit();
}
if($_SERVER['REQUEST_METHOD'] == "GET"){
    $catId = $_GET["catalogId"];
    $bo = new GenCatalogBO();
    if(!$bo->pullLocalCatalog2GenCat($catId)){
         //  print("pullLocalCatalog2GenCat Process Finished DONE");
        //file_put_contents($_SESSION["ROOT_PATH"]."/cache/version_cat_".$catId.".txt", "1");
        print(json_encode(array("success"=>false)));
    }
    $file = fopen($_SESSION["ROOT_PATH"]."/cache/version_cat_".$catId.".txt", "w");
    fwrite($file, "1");
    print(json_encode(array("success"=>true)));
}
