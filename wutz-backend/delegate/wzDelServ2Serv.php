<?php
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/WutzCatalogBO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/PlayManagerBO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';
include_once $_SESSION["ROOT_PATH"].'/common/TokensCont.php';

//print(" AuthToken ". $authToken);
//print_r($heds);
//session_sta
//rt();

class WzDelServ2Serv {
    public function dispatchRequest($heds,$fnc, $request) {
        //$heds = getallheaders();
        $authToken = isset($heds["Authorization"])?$heds["Authorization"]:"";
        header('Content-Type: application/json; charset=UTF-8');
        if($fnc === "addSongToQueue"){
            $this->addSongToQueue($request);
        }
        else if($fnc === "rescuePendingList"){
            $this->rescuePendingList($request, $authToken);
        }
        else if($fnc === "unqueueSong"){
            $this->unqueueSong($request, $authToken);
        }
        else if($fnc === "getFullCatalog"){
            $this->getFullCatalog($request, $authToken);
        }
        else if($fnc === "auth"){
            $this->auth($request);
        }
        else if($fnc === "getUpdatesVersion"){
            $this->getUpdatesVersion($request);
        }
        else if($fnc === "cleanCatalog"){
            $this->cleanCatalog($request, $authToken);
        }
        else if($fnc === "requestRecoverPassCode"){
            $this->requestRecoverPassCode($request);
        }
        else if($fnc === "sendPassRecoverPassCode"){
            $this->sendPassRecoverPassCode($request);
        }
        else if($fnc === "addCredits"){
            $this->addCredits($request, $authToken);
        }
        else if($fnc === "createTransfCode"){
            $this->createTransfCode($request, $authToken);
        }
        else if($fnc === "transferCredits"){
            $this->transferCredits($request);
        }
        else if($fnc === "refresh"){
            $this->refreshToken($request);
        }
    }
    //--------------------------------------------------------
    private function addSongToQueue($request){
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            $json = file_get_contents('php://input');
            $obj = json_decode($json, true);
            $catId = $obj["catId"];
            $token = $obj["token"];
            $songId = $obj["songId"];
            $guid = $obj["guid"];
        // $ip = $_SERVER['REMOTE_ADDR'];
            $managerBo = new PlayManagerBO();
            $res = $managerBo->addSongToPlayList($catId, $token, $guid, $songId);
            print json_encode($res);
        }
    }

    private function rescuePendingList($request, $authToken){
        if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
        print('{"error":{"code":1,"msg":"Not Authorized"}}');
        exit();
        }
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            $json = file_get_contents('php://input');
            $obj = json_decode($json, true);
            $catId = $obj["catId"];
            $token = $obj["token"];
            //$ip = $_SERVER['REMOTE_ADDR'];
            $managerBo = new PlayManagerBO();
            $res = $managerBo->rescuePendingList($token, $catId);
            print json_encode($res);
        }
    }

    private function unqueueSong($request, $authToken){
        if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
        print('{"error":{"code":1,"msg":"Not Authorized"}}');
        exit();
        }
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            $json = file_get_contents('php://input');
            $obj = json_decode($json, true);
            $catId = $obj["catId"];
            $token = $obj["token"];
            $songId = $obj["songId"];
            $guid = $obj["guid"];
            //$ip = $_SERVER['REMOTE_ADDR'];
            $managerBo = new PlayManagerBO();
            $res = $managerBo->removeSongFromList($token, $catId,$guid, $songId);
            print json_encode($res);
        }
    }

    private function getFullCatalog($request, $authToken){
        if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
        print('{"error":{"code":1,"msg":"Not Authorized"}}');
        exit();
        }
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                $json = file_get_contents('php://input');
                $obj = json_decode($json, true);
                $catId = $obj["catId"];
                $token = $obj["token"];
            // $guid = $obj["guid"];
                $ip = $_SERVER['REMOTE_ADDR'];
                $managerBo = new PlayManagerBO();
                $res = $managerBo->getPlayListSongs($token, $catId, false);

            //  print_r($res);

                print json_encode($res);
            }
    }

    private function auth($request){
        if($_SERVER['REQUEST_METHOD'] == "POST"){
                $json = file_get_contents('php://input');
                $obj = json_decode($json, true);
                $adminBo = new WutzAdminBO();
                $res;
                if(isset($obj["accTkn"]))
                    $res = $adminBo->loginWithaccTkn($obj["barid"], $obj["accTkn"]);
                else
                    $res = $adminBo->loggingCorrect($obj);
                if($res["logged"])
                    $_SESSION["barLoadedSess"] = $obj["barid"];
                print json_encode($res);
        }
    }

    private function refreshToken($request){
        if($_SERVER['REQUEST_METHOD'] == "POST"){
                $json = file_get_contents('php://input');
                $obj = json_decode($json, true);
                $adminBo = new WutzAdminBO();
                $res;
                if(isset($obj["sessToken"]))
                    $res = $adminBo->refreshToken($_SESSION["barLoadedSess"], $obj["sessToken"]);
                print json_encode($res);
        }
    }

    private function getUpdatesVersion($request){
        if($_SERVER['REQUEST_METHOD'] == "GET"){
            $sys = $request["sys"];
            $adminBo = new WutzAdminBO();
            $res = $adminBo->getUpdateVersion($sys);
            print $res;
        }
    }

    private function cleanCatalog($request, $authToken){
        if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
        print('{"error":{"code":1,"msg":"Not Authorized"}}');
        exit();
        }
        if($_SERVER['REQUEST_METHOD'] == "POST"){
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);
        $catId = $obj["catId"];
        $token = $obj["token"];
        $managerBo = new PlayManagerBO();
        $res = $managerBo->resetCatalogPlayList($token, $catId);
        print json_encode($res);
        }
    }
    private function requestRecoverPassCode($request){
        if($_SERVER['REQUEST_METHOD'] == "POST"){
        $barId = $request["barId"];
        $managerBo = new WutzAdminBO();
        $res = $managerBo->createRecoverPassCode($barId);
        print json_encode($res);
        }
    }

    private function sendPassRecoverPassCode($request){
        if($_SERVER['REQUEST_METHOD'] == "POST"){
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);
        $barId = $obj["barId"];
        $code = $obj["code"];
        $newPass = $obj["newPass"];
        $managerBo = new WutzAdminBO();
        $res = $managerBo->setNewBarPassword($barId, $code, $newPass);
        print json_encode($res);
        }
    }

    private function addCredits($request, $authToken){
        if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
        print('{"error":{"code":1,"msg":"Not Authorized"}}');
        exit();
        }
        if($_SERVER['REQUEST_METHOD'] == "POST"){
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);
        $barId = $obj["barId"];
        $catId = $obj["catId"];
        $guid = $obj["guid"];
        $credits = $obj["credits"];
        $managerBo = new WutzAdminBO();
        $res = $managerBo->addCreditsTo($barId, $guid, $catId, $credits);
        print json_encode($res);
        }
    }
    private function createTransfCode($request, $authToken){
        if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
        print('{"error":{"code":1,"msg":"Not Authorized"}}');
        exit();
        }
        if($_SERVER['REQUEST_METHOD'] == "POST"){
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);
        $catId = $obj["catId"];
        $guid = $obj["guid"];
        $credits = $obj["credits"];
        $managerBo = new WutzAdminBO();
        $res = $managerBo->createTransfCode($guid, $catId, $credits);
        print json_encode($res);
        }
    }
    private function transferCredits($request){
        if($_SERVER['REQUEST_METHOD'] == "POST"){
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);
        $catId = $obj["catId"];
        $transferCode = $obj["tranCode"];
        $guidTo = $obj["guidto"];
        $managerBo = new WutzAdminBO();
        $res = $managerBo->transferCredits($catId, $guidTo ,$transferCode);
        print json_encode($res);
        }
    }
}