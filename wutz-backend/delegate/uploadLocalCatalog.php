<?php
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';
include_once $_SESSION["ROOT_PATH"].'/common/TokensCont.php';
$heds = getallheaders();
$authToken = isset($heds["Authorization"])?$heds["Authorization"]:"";
header('Content-Type: application/json');
if(!TokensCont::checkAuthorization($_SESSION["barLoadedSess"],$authToken)){
   print('{"error":{"code":1,"msg":"Not Authorized"}}');
    exit();
}
if($_SERVER['REQUEST_METHOD'] == "POST"){
    $wutzAdminBo = new WutzAdminBO();
    $json = file_get_contents('php://input');
    $json = urldecode($json);
    $obj = json_decode($json, true);
     $cat = $wutzAdminBo->createCatalog($obj);
    print(json_encode(array("Transaction"=>"OK","catalog"=>$cat)));
}
