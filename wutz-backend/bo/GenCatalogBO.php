<?php
include_once $_SESSION["ROOT_PATH"].'/dao/GenCatalogDAO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';
include_once $_SESSION["ROOT_PATH"].'/common/LastFMAPI.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GetCatalogBO
 *
 * @author CRTOLEDO
 */
class GenCatalogBO {


    public function refreshGetCatalogFromLasfFM(){
        $genCatDao = new GenCatalogDAO();
        $allArtists = $genCatDao->getGenArtistWithNoImages();
        foreach ($allArtists as $artNode) {
            $artNode = $this->getArtistInfoFromLastFM($artNode);
            $genCatDao->setGenArtist($artNode);
        }
        $allAlbums = $genCatDao->getGenAlbumsWithNoImages();
        foreach ($allAlbums as $albNode) {
           $albNode = $this->getAlbumInfoFromLastFM($albNode);
           if(isset($albNode["lfm_img_url"])){
                $genCatDao->setGenAlbum($albNode);
           }
        }
        $genCatDao->cleanUpRowsWithNoImage();
        return true;
    }

    public function refreshAllArtistImgFromLasfFM(){
        $genCatDao = new GenCatalogDAO();
        $allArtists = $genCatDao->getAllGenArtists();
        foreach ($allArtists as $artNode) {
            $artNode = $this->getArtistInfoFromLastFM($artNode);
            $genCatDao->setGenArtist($artNode);
        }
        $genCatDao->cleanUpRowsWithNoImage();
        return true;
    }

    private function checkIfImageExist($imgUrl){
      $header_response = get_headers($imgUrl, 1);
      if ( strpos( $header_response[0], "404" ) !== false ) {
         return false;
      }
      else       {
         return true;
      }
    }

    private function getArtistInfoFromLastFM($artist){
        $lastFmApi = new LastFMAPI();
        $res = $lastFmApi->searchArtist($artist["name"]);
       // print json_encode($res)."<br/>";
        $artist["lfm_id"] = isset($res["artist"]["mbid"])?$res["artist"]["mbid"]:"-";
        if(isset($res["artist"])){
            foreach ($res["artist"]["image"] as $imgNode) {
                if($imgNode["size"] == "large"){
                    if($this->checkIfImageExist($imgNode["#text"]))
                        $artist["lfm_img_url"] = $imgNode["#text"];
                    break;
                }
            }
            $artist["lfm_json"] = json_encode($res);
        }
        return $artist;
    }

    private function getAlbumInfoFromLastFM($album){
        $lastFmApi = new LastFMAPI();
        $res = $lastFmApi->searchAlbum($album["artist_name"],$album["name"]);
        if(!isset($res["error"])){
            $album["lfm_id"] = isset($res["album"]["mbid"])?$res["album"]["mbid"]:"-";
            foreach ($res["album"]["image"] as $imgNode) {
                if($imgNode["size"] == "large"){
                   if($this->checkIfImageExist($imgNode["#text"]))
                    $album["lfm_img_url"] = $imgNode["#text"];
                    break;
                }
            }
            $album["lfm_json"] = json_encode($res);
        }
        return $album;
    }

    public function pullLocalCatalog2GenCat($catId){
        $genCatDao = new GenCatalogDAO();
        $genCatDao->pullLocalCatalog2GenCatalog($catId);

  //      $this->refreshGetCatalogFromLasfFM();

        return true;
    }

    public function refreshLastFMGenSync(){
        $genCatDao = new GenCatalogDAO();
        $this->refreshGetCatalogFromLasfFM();
        return true;
    }
}
