<?php
include_once $_SESSION["ROOT_PATH"].'/dao/ShowCatalogDAO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WutzCatalogBO
 *
 * @author CRTOLEDO
 */
class WutzCatalogBO {
private $config;
    public function WutzCatalogBO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }

    function getAllArtist($catId){

        $utils = new Utils();
        $dao = new ShowCatalogDAO();
        $res = $dao->getAllArtist($catId);
        if(sizeof($res)>0){
            //$res = $utils->utf8_encode_all($res);
            return $res;
        }
        return $res;
    }

    function getAlbumPerArtist($catId,$artistId){

        $utils = new Utils();
        $dao = new ShowCatalogDAO();
        $res = $dao->getArtistAlbumList($catId, $artistId);
        if(sizeof($res)>0){
           // $res = $utils->utf8_encode_all($res);
            return $res;
        }
        return $res;
    }
    function getAlbumSongs($catId,$albumId){

        $utils = new Utils();
        $dao = new ShowCatalogDAO();
        $res = $dao->getAlbumSongList($catId, $albumId);
        if(sizeof($res)>0){
            //$res = $utils->utf8_encode_all($res);
            return $res;
        }
        return $res;
    }

    function searchInCatalog($catId, $seTxt){
        $dao = new ShowCatalogDAO();
        $res = array();
        $albums = $dao->search4Albums($catId, $seTxt);
        if(sizeof($albums)>0){
            //$res = $utils->utf8_encode_all($res);
            $res["albums"] = $albums;
        }
        $songs = $dao->search4Songs($catId, $seTxt);
        if(sizeof($songs)>0){
            //$res = $utils->utf8_encode_all($res);
            $res["songs"] = $songs;
        }
        return $res;
    }

    function getArtistSongs($catId, $artId){
      $dao = new ShowCatalogDAO();
      $res = $dao->getSongs4Artist($catId, $artId);
      if(sizeof($albums)>0){
          //$res = $utils->utf8_encode_all($res);
          return $res;
      }
      return $res;
    }

}
