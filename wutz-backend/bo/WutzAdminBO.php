<?php
include_once $_SESSION["ROOT_PATH"].'/dao/WutzAdminDAO.php';
include_once $_SESSION["ROOT_PATH"].'/dao/PlayManagerDAO.php';
include_once $_SESSION["ROOT_PATH"].'/dao/LoadCatalogDAO.php';
include_once $_SESSION["ROOT_PATH"].'/dao/TknManagerDAO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';
include_once $_SESSION["ROOT_PATH"].'/common/TokensCont.php';



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WutzAdminBO
 *
 * @author CRTOLEDO
 */
class WutzAdminBO {

    private $config;
    private $locales;
    public function WutzAdminBO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $localeCont = file_get_contents($_SESSION["ROOT_PATH"]."/locales/es.json");
        $this->config = json_decode($confCont, true);
        $this->locales = json_decode($localeCont, true);
    }

    public function searchBar($barId){

        if($barId == ""){
            return array();
        }

        $utils = new Utils();
        $wutzDao = new WutzAdminDAO();
        $res = $wutzDao->searchBar($barId);

        //print_r($res);

        if(sizeof($res)>0){
            //$res = $utils->
            return $res;
        }
        return $res;
    }

    public function getBarDetails($barId){

        $utils = new Utils();
        $wutzDao = new WutzAdminDAO();
        $res = $wutzDao->getBarAndCatDetails($barId);
        if(sizeof($res)>0){
            $res = $res[0];
        }
        return $res;
    }

    public function isThisDayToken($catId,$token){

        $utils = new Utils();
        $wutzDao = new WutzAdminDAO();
        $res = $wutzDao->getBarDayToken($catId);
        if(sizeof($res)>0){
            $res = $utils->utf8_encode_all($res[0]);
            $catToken = $res["dayToken"];
            if($catToken == $token){
                return true;
            }
        }
      return false;
    }

    public function connect2Bar($catId,$guid,$dayToken){
        $utils = new Utils();
        $wutzDao = new WutzAdminDAO();
        $playDao = new PlayManagerDAO();
        $res = $wutzDao->getBarDayToken($catId);
        if(sizeof($res)>0){
            $res = $utils->utf8_encode_all($res[0]);
            $catToken = $res["dayToken"];
            if($catToken == $dayToken){
                $client = $playDao->getClient($catId, $guid);
                //print_r($client);
                if(!isset($client) || !$client){
                      if(!$playDao->addClient($catId, $guid))
                         return array("OK"=>true);
                }
                return array("OK"=>true);
            }
        }
      return array("OK"=>false);
    }

    public function getNearByBars($usrLat,$usrLong){

        $utils = new Utils();
        $wutzDao = new WutzAdminDAO();
        $res = $wutzDao->getNearByBars($usrLat, $usrLong);
        if(sizeof($res)>0){
           // $res = $utils->utf8_encode_all($res);
            return $res;
        }
        else{
            return false;
        }
    }

    public function createCatalog($barArrCat){

        $lCatDao = new LoadCatalogDAO();
        $barID = $barArrCat["bar_id"];

        $lCatDao->setBarStatus($barID, "N");
        $catalog = $lCatDao->createGetCatalog($barID);
        $catId = $catalog["cats"][0]["idcatalog"];

        if($catalog["status"] === "EXIST"){
            $lCatDao->removeCatalog($catId);
            $this->removeCatalogFromCache($catId);
            $catalog = $lCatDao->createGetCatalog($barID);
            $catId = $catalog["cats"][0]["idcatalog"];
        }

       $artist = $this->catchArtists($barArrCat["songs"]);
       $artist = $lCatDao->insertBulkArtist($catId, $artist);
       $albums = $this->catchAlbums($artist, $barArrCat["songs"]);
       $albums = $lCatDao->insertBulkAlbums($catId, $albums);
       $songs = $this->mergeSongAlbum($albums, $barArrCat["songs"]);
       $songs = $lCatDao->insertBulkSongs($catId, $songs);
        $lCatDao->setBarStatus($barID, "Y");
        return $catalog;

    }

    public function updateCatalog($catId, $barArrCat){
        $utils = new Utils();
        $lCatDao = new LoadCatalogDAO();
        $barID = $barArrCat["bar_id"];
        $lCatDao->setBarStatus($barID, "N");
        $catArtists = $lCatDao->getCatalogArtist($catId);
        $catAlbums = $lCatDao->getCatalogAlbums($catId);

        $catalog = $lCatDao->createGetCatalog($barID);
        $artist = $this->catchArtists($barArrCat["songs"]);
        $artist = $utils->diffFilterArray("name", $artist, array_column($catArtists, "name"));
        if(sizeof($artist) > 0){
            $artist = $lCatDao->insertBulkArtist($catId, $artist);
        }
        else{
            $artist = array_merge($catArtists, $artist);
        }
       $albums = $this->catchAlbums($artist, $barArrCat["songs"]);
       $albums = $utils->diffFilterArray("albumname", $albums, array_column($catAlbums, "name"));
       if(sizeof($albums) > 0){
            $albums = $lCatDao->insertBulkAlbums($catId, $albums);
        }
        else{
            $albums = array_merge($catAlbums, $albums);
        }
       $songs = $this->mergeSongAlbum($albums, $barArrCat["songs"]);
       $songs = $lCatDao->insertBulkSongs($catId, $songs);
        $lCatDao->setBarStatus($barID, "Y");
        return $catalog;

    }

    function saveLocalBarInfo($barInfo){

        $admDao = new WutzAdminDAO();
        if(!isset($barInfo["songsAllowed"])){
            $barInfo["songsAllowed"] = 3;
        }
        $res = $admDao->saveBar($barInfo["bar_id"], $barInfo);

        if($res){
            return true;
        }
        else {
            return false;
        }
    }

    function registerBar($barInfo){

        $admDao = new WutzAdminDAO();
        $res = $admDao->registerBar($barInfo["bar_id"], $barInfo);
       //$res = $admDao->createUpdateBar($barInfo["bar_id"], $barInfo);
        if($res > 0){
            $this->sendNewRegisterEmail2Admin($barInfo);
            return true;
        }
        else {
            return false;
        }
    }



    function getBarConfig4Cat($catId){
        $admDao = new WutzAdminDAO();
        $res = $admDao->getBarConfig4Cat($catId);
        if(sizeof($res) > 0){
            $res = $res[0];
            $res["superClient"] = json_decode($res["superClient"]);
        }
        return $res;
    }

    //-- Private Catalog functions
   private function catchArtists($songs){
       $artistList = array();
       foreach($songs as $song){
           if(!in_array($song["songArtist"], array_column($artistList, "name"))){
               array_push($artistList,array("name"=>$song["songArtist"]));
           }
       }
       return $artistList;
   }

   private function catchAlbums($artist, $songs){
       $albumList = array();
       foreach($artist as $art) {
           foreach($songs as $song) {
                if($song["songArtist"] == $art["name"]){
                    $tmpArr = array();
                    $tmpArr["idartist"] = $art["idartist"];
                    $tmpArr["albumname"] = $song["songAlbum"];
                    $tmpArr["artName"] = $art["name"];
                    //$tmpArr["albumname"] = str_replace($song["songArtist"],"",$tmpArr["albumname"]);
                    if(!in_array($tmpArr, $albumList)){
                       array_push($albumList,$tmpArr);
                    }
                }
           }
       }
       return $albumList;
   }

   private function mergeSongAlbum($albumList, $songs){
      $newSongList = array();

      //print_r($albumList);

       foreach($songs as $song){
           foreach($albumList as $alb){
               if($song["songArtist"] == $alb["artistName"]  && $song["songAlbum"] == $alb["name"]){
                   $song["idalbum"] = $alb["idalbum"];
                   if(!in_array($song,$newSongList))
                       array_push ($newSongList,$song);
               }
           }
       }
      return $newSongList;
   }

   //Login
   public function loggingCorrect($acc){
       $admDao = new WutzAdminDAO();
       $barId = $acc["barid"];
       $pass = md5($acc["pass"]);
       $rec = $admDao->getBarPass($barId);
       if(sizeof($rec)>0){
           if($pass == $rec[0]["pass"]){
               $retVal = array("logged"=>true);
               $barDet = $admDao->getBarAndCatDetails($barId)[0];
               $barDayToken = isset($admDao->getBarDayToken($barDet["idcatalog"])[0])?$admDao->getBarDayToken($barDet["idcatalog"])[0]["dayToken"]:"";
               $barDet["dayToken"] = $barDayToken;
               $retVal["barDet"] = $barDet;
               $retVal["accTkn"] = md5($pass.$barId.$pass);
               $token = bin2hex(openssl_random_pseudo_bytes(64));
              // $token = "44444";
               $retVal["token"] = $token;
               TokensCont::setToken($barId, $token);
               return $retVal;
           }
           else{
               return array("logged"=>false,"msg"=>"wrong_pass");
           }
       }
       else{
           return array("logged"=>false,"msg"=>"usr_no_exist");
       }
   }

   //Login
   public function loginWithaccTkn($barId, $acc){
       $admDao = new WutzAdminDAO();
      // $barId = $acc["barid"];
      // $pass = md5($acc["pass"]);
       $rec = $admDao->getBarPass($barId);
      // print_r($rec);
       if(sizeof($rec)>0){
          $pass = $rec[0]["pass"];
          $servAcc = md5($pass.$barId.$pass);
           if($servAcc == $acc){
               $retVal = array("logged"=>true);
               $barDet = $admDao->getBarAndCatDetails($barId)[0];
               $barDayToken = isset($admDao->getBarDayToken($barDet["idcatalog"])[0])?$admDao->getBarDayToken($barDet["idcatalog"])[0]["dayToken"]:"";
               $barDet["dayToken"] = $barDayToken;
               $retVal["barDet"] = $barDet;
            //   $retVal["accTkn"] = md5($pass.$barId.$pass);
               $token = bin2hex(openssl_random_pseudo_bytes(64));
              // $token = "44444";
               $retVal["token"] = $token;
               TokensCont::setToken($barId, $token);
               return $retVal;
           }
           else{
               return array("logged"=>false,"msg"=>"wrong_pass");
           }
       }
       else{
           return array("logged"=>false,"msg"=>"usr_no_exist");
       }
   }

   public function refreshToken($barId,$authToken){
     $retVal = array("OK"=>false);
     if(TokensCont::checkAuthorization($barId,$authToken)){
       $token = bin2hex(openssl_random_pseudo_bytes(64));
       $retVal["OK"] = true;
       $retVal["token"] = $token;
       TokensCont::setToken($barId, $token);
     }
     return $retVal;
   }

   private function sendNewRegisterEmail2Admin($newBar){
      $mailList =  $this->config["mailList"];
      $subject = "Nuevos Bares/Eventos Registrados";
      $datosBar = array("Bar Id"=>$newBar["bar_id"],"Nombre Bar"=>$newBar["nombreBar"],"Email"=>$newBar["email"]);
      $message = json_encode($datosBar);
      $utils = new Utils();
      $utils->sendEmail($mailList, $subject, $message);
   }

   public function getUpdateVersion($sys){
       $updateCont = file_get_contents($_SESSION["ROOT_PATH"]."/apps/updates/newUpdate.json");
       $updateArr = json_decode($updateCont,true);
       //print_r($updateArr);
       return json_encode($updateArr[$sys]);
   }

   public function removeCatalogFromCache($catId){
       $line = 0;
       $initPath = $_SESSION["ROOT_PATH"]."/cache";
       $it = new RecursiveDirectoryIterator($initPath);
       foreach(new RecursiveIteratorIterator($it) as $file) {
           $fileName = $file->getFileName();
           $fullFilePath = $file->getPath();
           if($fileName !== "." && $fileName !== ".."){
                if(strpos($fileName, "_".$catId."_")==0){
                    //$pushOut["line_".$line++] = "To Remove : ".$fullFilePath."/".$fileName;
                     unlink($fullFilePath."/".$fileName);
                }
           }
       }
   }

   public function createRecoverPassCode($barId){
     $code = rand(10000000 , 99999999);
     $bar = $this->getBarDetails($barId);
     TokensCont::setToken("recPass_".$barId, $code);
  //   print "send mail to [".$bar["email"]."] with [$code]";
      $utils = new Utils();
      $mailList = $bar["email"];//array($bar["email"]);
      $subject = $this->locales["recPassSubject"];
      $message = "<p>".str_replace("__CODE__", $code, $this->locales["recPassBody"])."</p>";
      $utils->sendEmail($mailList, $subject, $message);
     return array("OK" => true);
   }

   public function setNewBarPassword($barId, $code, $newPass){
     $allOK = true;
      if(TokensCont::checkAuthorization("recPass_".$barId,$code)){
          $admDao = new WutzAdminDAO();
          if($admDao->setBarPass($barId,$newPass)){
              return array("OK" => true);
          }
          return array("OK" => false,"msg"=>"PassNoUpd");
      }
      return array("OK" => false,"msg"=>"badCode");
   }

   public function addCreditsTo($barId, $guid, $catId, $credits){
     $playDao = new PlayManagerDAO();
     $admDao = new WutzAdminDAO();
     $client = $playDao->getClient($catId,$guid);
     $client["credits"] += $credits;
     $playDao->updateClient($catId, $client);
     $admDao->sumCredits2Bar($barId, $credits);
     return array("OK" => true,"totalCredits" => $client["credits"]);
   }
   public function createTransfCode($guid, $catId, $credits){
        $trCode = bin2hex(openssl_random_pseudo_bytes(64));
        $tknMan = new TknManagerDAO();
        if($tknMan->setToken($trCode, $catId."_".$guid."_".$credits)){
           return array("OK" => true,"transfCode" => $trCode);
        }
        return array("OK" => false,"msg" => "Token Not Created For Some Reason");
      //  TokensCont::setToken($trCode, $catId."_".$guid."_".$credits);

   }

   public function transferCredits($catId, $guidTo ,$transferCode){
       $tknMan = new TknManagerDAO();
       $transInfo = $tknMan->consumeToken($transferCode);
    //   $transInfo = TokensCont::getToken($transferCode);
    //   print_r($transInfo);
       if(isset($transInfo)){
          $pieces = explode("_", $transInfo);
          $catId = $pieces[0];
          $guidFrom = $pieces[1];
          $credits = $pieces[2];
          TokensCont::removeToken($transferCode);
          $playDao = new PlayManagerDAO();
          $clientFrom = $playDao->getClient($catId,$guidFrom);
          $clientTo = $playDao->getClient($catId,$guidTo);
          if($clientFrom["credits"] >= $credits){
              $clientFrom["credits"] -= $credits;
              $clientTo["credits"] += $credits;
              $okOne = $playDao->updateClient($catId, $clientFrom);
              $okTwo = $playDao->updateClient($catId, $clientTo);
              return array("OK" => ($okOne && $okTwo),"msg"=>"transfer_finished");
          }
          else{
             return array("OK" => false,"msg"=>"no_enough_credits");
          }
       }
       else{
          return array("OK" => false,"msg"=>"no_transfer");
       }
   }
}
