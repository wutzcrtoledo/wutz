<?php


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GuardBO
 *
 * @author cristian
 */

class GuardBO {

    private $config;
    public function GuardBO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }

    public function canAddSongForCounts($client, $barConfig){

        $maxSongs = isset($barConfig["songsAllowed"])?$barConfig["songsAllowed"]:3;
        $supCl = isset($barConfig["superClient"])?$barConfig["superClient"]:array([]);
        if($maxSongs === 0 || in_array($client["guid"], $supCl) || in_array($client["ip"], $supCl))
            return true;
        else{
            $songsQueue = $client["songs_in_queue"];
            if($songsQueue >= $maxSongs && $client["credits"] <= 0)
                return false;
            else
                return true;
        }
    }


}
