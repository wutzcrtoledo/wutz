<?php
include_once $_SESSION["ROOT_PATH"].'/dao/FacturacionDAO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FacturacionBO
 *
 * @author cristian
 */

class FacturacionBO {

    private $config;
    public function FacturacionBO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }

    public function facturarMes(){
        $facDao = new FacturacionDAO();
        try{
          $facDao->crearNuevasFacturasEsteMes();
          $facDao->aplicarInteresEnMora();
          return array("OK"=>true);
        }
        catch(Exception $ex){
            return array("OK"=>false,"err"=>$ex);
        }
    }
}
