<?php
include_once $_SESSION["ROOT_PATH"].'/dao/PlayManagerDAO.php';
include_once $_SESSION["ROOT_PATH"].'/common/Utils.php';
include_once $_SESSION["ROOT_PATH"].'/bo/GuardBO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlayManagerBO
 *
 * @author CRTOLEDO
 */
class PlayManagerBO {
    //put your code here

  public function addSongToPlayList($catId, $token, $guid, $songId)  {
      $msg = array();
      $adminBO = new WutzAdminBO();
      if(!$adminBO->isThisDayToken($catId, $token)){
          $msg["added"] = "NO";
          $msg["msg"] = "exptoken";
          return $msg;
      }

      $barConfig = $adminBO->getBarConfig4Cat($catId);
      $allOk = true;
      $playDao = new PlayManagerDAO();
      $guard = new GuardBO();

      $client = $playDao->getClient($catId,$guid);
      if(!$client) {
          $allOk =  $playDao->addClient($catId,$guid);
          $client = $playDao->getClient($catId,$guid);
      }

      if($songId == -1) {
          $songId = $playDao->getRandomSongId($catId);
          $allOk = $playDao->addToPlayList($catId, $guid ,$songId);
          $msg["added"] = "OK";
          $msg["msg"] = "randadd";
          return $msg;
      }
      if($playDao->isSongInPlaylist($catId, $songId)){
          $msg["added"] = "NO";
          $msg["msg"] = "repeated";
          return $msg;
      }
      if($allOk)
      {
        if($guard->canAddSongForCounts($client, $barConfig)){
            $allOk = $playDao->addToPlayList($catId,$guid, $songId);
            if($allOk){
                $msg["added"] = "OK";
                $msg["msg"] = "added";
                $client["songs_in_queue"] = intval($client["songs_in_queue"]);
                $client["songs_in_queue"]++;
                $client["credits"]--;
                $allOk = $playDao->updateClient($catId, $client);
            }
            else{
                $msg["msg"] = "added_no_client";
            }
        }
        else{
            $msg["added"] = "NO";
            $msg["msg"] = "added_max_songs";
            return $msg;
        }
      }
      return $msg;
  }
  //-------------------------------------------------------------

  public function getPlayListSongs($token, $catId, $readOnly)  {
      $adminBO = new WutzAdminBO();
      if(!$adminBO->isThisDayToken($catId, $token)){
          $msg["access"] = "NO";
          $msg["msg"] = "Token not in sync";
          return $msg;
      }
      $playDao = new PlayManagerDAO();
      $utils= new Utils();
      $res = array();
      if(!isset($readOnly) || !$readOnly)
        $res = $playDao->getPlayListSongs($catId);
      else
        $res = $playDao->getPlayListSongsReadOnly($catId);
      //print_r($res);
      //$res = $utils->utf8_encode_all($res);
      //print_r($res);

      // $res = $utils->getHTMLArray($res);
      return $res;
  }

  public function removeSongFromList($token, $catId,$guid, $songid)  {
      $adminBO = new WutzAdminBO();
      if(!$adminBO->isThisDayToken($catId, $token)){
          $msg["access"] = "NO";
          $msg["msg"] = "Token not in sync";
          return $msg;
      }
      $playDao = new PlayManagerDAO();
      $client = $playDao->getClient($catId, $guid);
      if($client){
          $client["songs_in_queue"] = intval($client["songs_in_queue"]);
          if($client["songs_in_queue"] > 0){
            $client["songs_in_queue"]--;
          }
          $playDao->updateClient($catId, $client);
      }
      return $playDao->removeSongFromPlayList($catId, $guid, $songid);
  }

  public function resetCatalogPlayList($token, $catId){
      $adminBO = new WutzAdminBO();
      if(!$adminBO->isThisDayToken($catId, $token)){
          $msg["access"] = "NO";
          $msg["msg"] = "Token not in sync";
          return $msg;
      }
      $playDao = new PlayManagerDAO();
      $res = array("OK" => $playDao->cleanCatalogPlayList($catId));
      return  $res;
  }

  public function rescuePendingList($token, $catId){
      $adminBO = new WutzAdminBO();
      if(!$adminBO->isThisDayToken($catId, $token)){
          $msg["access"] = "NO";
          $msg["msg"] = "Token not in sync";
          return $msg;
      }
      $playDao = new PlayManagerDAO();
      return $playDao->resetPlayList($catId);
  }
}
