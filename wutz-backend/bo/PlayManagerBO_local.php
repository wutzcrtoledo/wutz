<?php
include_once $_SESSION["ROOT_PATH"].'/serv/dao/PlayManagerDAO.php';
include_once $_SESSION["ROOT_PATH"].'/serv/dto/AudioItemDTO.php';
include_once $_SESSION["ROOT_PATH"].'/serv/dto/ClientDTO.php';
include_once $_SESSION["ROOT_PATH"].'/serv/common/Utils.php';
include_once $_SESSION["ROOT_PATH"].'/serv/bo/GuardBO.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlayManagerBO
 *
 * @author cristian
 */
class PlayManagerBO 
{
    
   public function getAndLoadSong($songId)
   {
      $utils = new Utils();
      $playDao = new PlayManagerDAO();
      $song = null;
      //$song = new AudioItemDTO();
      $song = $playDao->getSongInfo($songId);
      
      //print_r($song);
      
        try
        {
          if(isset($_SESSION["lastPlayed"]) && file_exists($_SESSION["lastPlayed"]) && $_SESSION["lastPlayed"] !== $_SESSION["ROOT_PATH"]."/audio/")
            unlink($_SESSION["lastPlayed"]);

        }catch (Exception $ex) {

        }
      $_SESSION["lastPlayed"] = $_SESSION["ROOT_PATH"]."/audio/currSong.mp3";
      
      //$filPath = htmlspecialchars($song["file_path"]);
      //$filName = htmlspecialchars($song["file_name"]);
      $old = $song["file_path"]."/".$song["file_name"];
      
      //print($old);
      
      $new = $_SESSION["ROOT_PATH"]."/audio/currSong.mp3";
      copy($old, $new) or die("Unable to copy $old to $new.");
      
     // $song["file_path"]= htmlspecialchars($song["file_path"]);
     // $song["file_name"]= htmlspecialchars($song["file_name"]);
     //print_r($song);
      return $utils->getHTMLArray($song);
   }
   
   public function getPlayListSongs()
   {
       $playDao = new PlayManagerDAO();
       $utils= new Utils();
       $res = $playDao->getPlayListSongs();
       
       $res = $utils->getHTMLArray($res);
       return $res;
   }
   
   public function loadCatalogToDatabase($path)
   {
       set_time_limit(0);
       $playDao = new PlayManagerDAO();
       $playDao->removeAllCatalog();
       $songs = $this->getSongsFromFileSystem($path);
       $artist = $this->catchArtists($songs);
       $artist = $playDao->insertBulkArtist($artist);
       $albums = $this->catchAlbums($artist, $songs);
       $albums = $playDao->insertBulkAlbums($albums);
      
       $songs = $this->mergeSongAlbum($albums, $songs);
       
      // print_r($songs);
       if($playDao->insertBulkSongs($songs))
       {
            set_time_limit(150);
           return true;
       }
       else{
           set_time_limit(150);
           return false;
       }
           
       //print_r($artist);
       //print_r($albums);
       
      // print_r($songs);
       
   }
   
   private function catchArtists($songs)
   {
       $artistList = array();
       foreach($songs as $song)
       {
           if(!in_array($song["songArtist"], $artistList))
           {
               array_push($artistList,$song["songArtist"]);
           }
       }
       return $artistList;
   }
   
   private function catchAlbums($artist, $songs)
   {
       $albumList = array();
       
       foreach($artist as $art)
       {
           foreach($songs as $song)
           {
                if($song["songArtist"] == $art["name"])
                {
                    $tmpArr = array();
                    $tmpArr["idartist"] = $art["idartist"];
                    $tmpArr["albumname"] = $song["songAlbum"];
                    
                    $tmpArr["artName"] = $art["name"];
                    
                    //$tmpArr["albumname"] = str_replace($song["songArtist"],"",$tmpArr["albumname"]);
                    if(!in_array($tmpArr, $albumList)) 
                    {
                       array_push($albumList,$tmpArr);    
                    }                    
                }
           }       
       }      
       
       return $albumList;
   }
   
   private function mergeSongAlbum($albumList, $songs)
   {
      $newSongList = array();
       foreach($songs as $song)
       {
           foreach($albumList as $alb)
           {
               if($song["songArtist"] == $alb["artistName"]  && $song["songAlbum"] == $alb["albumName"])
               {
                   $song["idalbum"] = $alb["idalbum"];
                   if(!in_array($song,$newSongList))
                       array_push ($newSongList,$song);
               }
           }
       }
       
      return $newSongList;
   }
   
   
   private function getSongsFromFileSystem($initPath)
   {        
            $utils = new Utils();
            $songs = array();
            $songImages = array();
            $it = new RecursiveDirectoryIterator($initPath);
            $display = Array ( 'mp3' );
            $imgTypes = Array('jpg','png');
            foreach(new RecursiveIteratorIterator($it) as $file) {
                
                if (in_array(strtolower(array_pop(explode('.', $file))), $display))
                {
                    $tempSong = array();
                    $fullFilePath = $file->getPath();
                    $sep = $_SESSION["CONFIG"]["separator"];
                    $songPath = str_replace($initPath,"",$fullFilePath); 
                    $metaDataArr = preg_split("/[\\".$sep."]+/", $songPath);
                    $songArtist = ($metaDataArr[sizeof($metaDataArr)-2] !== "")?$metaDataArr[sizeof($metaDataArr)-2]:$metaDataArr[sizeof($metaDataArr)-1];
                    $songAlbum = ($metaDataArr[sizeof($metaDataArr)-2] !== "")?$metaDataArr[sizeof($metaDataArr)-1]:"Unknown";
                    
                    $tempSong["songAlbum"] = $songAlbum;
                    $tempSong["songArtist"] = $songArtist;
                    $tempSong["songFileName"] = $file->getFileName();
                    $tempSong["songPath"] = $fullFilePath;
                    
                    $tags = $utils->getMP3Details($fullFilePath.$sep.$file->getFileName());
                    if(isset($tags["title"]) && $tags["title"]!==""){
                        $tempSong["songName"] = $tags["title"];
                        $tempSong["track"] = $tags["track"];
                    }
                    else{
                        $songName = str_replace(".mp3","",$tempSong["songFileName"]);
                        $songName = str_replace("-"," ",$songName);
                        $songName = str_replace("_"," ",$songName);
                        $songName = str_ireplace($tempSong["songArtist"],"",$songName);
                        $tempSong["songName"] = htmlspecialchars($songName);
                    }
                    if (isset($tags["binImg"]) && $tags["binImg"]!== ""){
                        
                        $newImg = "audio/fronts/".strtolower($songArtist)."_".strtolower($songAlbum).".jpg";
                        $songImages[strtolower($songArtist)."_".strtolower($songAlbum)] = $newImg;
                        @file_put_contents($_SESSION["ROOT_PATH"]."/".$newImg, $tags["binImg"]);
                    }
                    //$tempSong["songFileName"] = htmlspecialchars($tempSong["songFileName"]);
                    //$tempSong["songPath"] = htmlspecialchars($tempSong["songPath"]);
                    array_push($songs, $tempSong);
                }
                elseif (in_array(strtolower(array_pop(explode('.', $file))), $imgTypes)){
                    
                    if(strrpos(strtolower($file->getFileName()) , "front")>=0 || strrpos(strtolower($file->getFileName()) , "folder")>=0){
                    
                        $tempImg = array();
                        $fullFilePath = $file->getPath();
                        $sep = $_SESSION["CONFIG"]["separator"];
                        $imgPath = str_replace($initPath,"",$fullFilePath); 
                        $metaDataArr = preg_split("/[\\".$sep."]+/", $imgPath);
                        $imgArtist = ($metaDataArr[sizeof($metaDataArr)-2] !== "")?$metaDataArr[sizeof($metaDataArr)-2]:$metaDataArr[sizeof($metaDataArr)-1];
                        $imgAlbum = ($metaDataArr[sizeof($metaDataArr)-2] !== "")?$metaDataArr[sizeof($metaDataArr)-1]:"Unknown";
                        
                        $old = $fullFilePath.$sep.$file->getFileName();
                        $ext = pathinfo($old, PATHINFO_EXTENSION);
                        $new = $_SESSION["ROOT_PATH"]."/audio/fronts/".strtolower($imgArtist)."_".strtolower($imgAlbum).".".$ext;
                        copy($old, $new);
                        
                        $songImages[strtolower($imgArtist)."_".strtolower($imgAlbum)] = "audio/fronts/".strtolower($imgArtist)."_".strtolower($imgAlbum).".".$ext;
                    }
                }
            }
            
            //print_r($songImages);
            
            foreach ($songs as $idx => $songNode) {
                
                $imgId = strtolower($songNode["songArtist"])."_".strtolower($songNode["songAlbum"]);
                if(isset($songImages[$imgId])){
                    $songs[$idx]["pic"] = $songImages[$imgId];
                }
                else{
                    $songs[$idx]["pic"] = "";
                }
            }
            
           // print_r($songs);
            
            return $songs;
  }
  //----------------------------------------------------------------------------
  
  public function addSongToPlayList($ip,$songId)
  {
      $allOk = true;
      $playDao = new PlayManagerDAO();
      $guard = new GuardBO();
      
      $msg = array();
      
      if($songId == -1)
      {
          $songId = $playDao->getRandomSongId();
          $allOk = $playDao->addToPlayList($ip, $songId);
          $msg["added"] = "OK";
          $msg["msg"] = "randadd";
          return $msg;
      }
      
      $client = $playDao->getClient($ip); 
      if(!$client) {
          $allOk =  $playDao->addClient($ip);
          $client = $playDao->getClient($ip);
      }
      if($playDao->isSongInPlaylist($songId)){
          $msg["added"] = "NO";
          $msg["msg"] = "repeated";
          return $msg;
      }
      if($allOk)
      {
        if($guard->canAddSongForCounts($client)){   
            $allOk = $playDao->addToPlayList($ip, $songId);
            if($allOk){
                $msg["added"] = "OK";
                $msg["msg"] = "added";
                $client["songs_in_queue"] = intval($client["songs_in_queue"]);
                $client["songs_in_queue"]++;
                $allOk = $playDao->updateClient($client);
            }
            else{
                $msg["msg"] = "added_no_client";
            }
        }
        else{
            $msg["added"] = "NO";
            $msg["msg"] = "added_max_songs";
            return $msg;
        }
      } 
      return $msg;
  }
  
  public function rescuePendingList(){
      $playDao = new PlayManagerDAO();
      return $playDao->resetPlayList();
  }
  
  public function removeSongFromList($songid,$ip)
  {
      $playDao = new PlayManagerDAO();
      $client = $playDao->getClient($ip); 
      if($client){
          $client["songs_in_queue"] = intval($client["songs_in_queue"]);
          $client["songs_in_queue"]--;
          $playDao->updateClient($client);
      }
      return $playDao->removeSongFromPlayList($songid,$ip);
  }
  
  public function getAllArtist()
  {
      $playDao = new PlayManagerDAO();
      return $playDao->getAllArtist();
  }
  
  public function getArtistAlbum($artistId)
  {
      $playDao = new PlayManagerDAO();
      return $playDao->getArtistAlbumList($artistId);
  }
  
  public function getAlbumSongs($albumId)
  {
      $playDao = new PlayManagerDAO();
      return $playDao->getAlbumSongList($albumId);
  }
}
