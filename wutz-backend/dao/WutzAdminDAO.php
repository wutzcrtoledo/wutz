<?php
include_once  $_SESSION["ROOT_PATH"].'/common/ClassMySqlDB.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WutzAdminDAO
 *
 * @author CRTOLEDO
 */
class WutzAdminDAO {

    private $config;
    public function WutzAdminDAO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }
    public function searchBar($barId)
    {
       $dbObject = new ClassMySqlDB();
       $sql = "SELECT id,local_host_url
                FROM bar
                WHERE
                (lower(id) like CONCAT('%',lower('$barId'),'%') OR
                 lower(nombreBar) like CONCAT('%',lower('$barId'),'%'))
                and available_yn='Y'
                and lower(status) in ('active','new','free')";
     //  $sql = "SELECT id,local_host_url FROM bar WHERE  lower(id) like CONCAT('%',lower('$barId'),'%') and available_yn='Y' and status in ('active','New')";

       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }


    public function getNearByBars($usrLat,$usrLong)
    {
       $ratio = $this->config["GPSRadio"];
       $dbObject = new ClassMySqlDB();

       $sql = "select id,
                      representante,
                      telefono,
                      email,
                      latitude as lat,
                      longitute as lon
                from bar
                where latitude is not null
                and   longitute is not null
                and ABS(cast(latitude AS DECIMAL(10,5))) between (ABS($usrLat)-$ratio) and (ABS($usrLat)+$ratio)
                and ABS(cast(longitute AS DECIMAL(10,5))) between (ABS($usrLong)-$ratio) and (ABS($usrLong)+$ratio)
                and lower(status) in ('active','new','free') ";


       //print_r($sql);
       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }



    public function getBarAndCatDetails($barId){

       $dbObject = new ClassMySqlDB();

       $sql = "SELECT   b.id,
                        b.representante,
                        b.telefono,
                        b.email,
                        IFNULL(c.idcatalog,'') as idcatalog,
                        IFNULL(c.name,'') as catalog_name,
                        b.latitude as lat,
                        b.longitute as lon,
                        b.desc,
                        b.nombreBar,
                        b.songsAllowed,
                        b.available_yn
                 FROM bar b
                 LEFT JOIN catalog c
                 ON b.id = c.bar_id
                 WHERE b.id = '".$dbObject->escape_string($barId)."'
                  limit 1";

       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }

    public function getBarDayToken($catId){

       $dbObject = new ClassMySqlDB();
       $sql="select b.id,
                    b.dayToken,
                    c.idcatalog
             from bar b, catalog c
             where b.id = c.bar_id
             and c.idcatalog = '".$dbObject->escape_string($catId)."'
             limit 1";
       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }

    public function createUpdateBar($barId,$info){

       $dbObject = new ClassMySqlDB();
       $sql ="select count(1) as count from bar where id = '".$dbObject->escape_string($barId)."'";
       $res = $dbObject->getArrayFromQuery($sql);
       $dbObject = new ClassMySqlDB();
    if($res[0]["count"] == 0){
         $sql = "INSERT INTO bar (id, "
                             . "representante, "
                             . "telefono, "
                             . "email, "
                             . "latitude, "
                             . "longitute,"
                             . "dayToken, "
                             . "`desc`, "
                             . "nombreBar, "
                             . "songsAllowed, "
                             . "superClient) "
           . "VALUES   ('".$dbObject->escape_string($barId)."', "
                    . "'".$dbObject->escape_string($info["representante"])."', "
                    . "'".$dbObject->escape_string($info["telefono"])."', "
                    . "'".$dbObject->escape_string($info["email"])."', "
                    . "'".$dbObject->escape_string($info["latitude"])."', "
                    . "'".$dbObject->escape_string($info["longitute"])."', "
                    . "'".$dbObject->escape_string($info["dayToken"])."', "
                    . "'".$dbObject->escape_string($info["desc"])."', "
                    . "'".$dbObject->escape_string($info["nombreBar"])."', "
                    . "'".$dbObject->escape_string($info["songsAllowed"])."', "
                    . "'".$dbObject->escape_string(json_encode($info["superClient"]))."')";
    }
    else{

       $sql = "UPDATE bar SET "
               . "representante = '".$dbObject->escape_string($info["representante"])."', "
               . "telefono = '".$dbObject->escape_string($info["telefono"])."', "
               . "email = '".$dbObject->escape_string($info["email"])."', "
               . "latitude = '".$dbObject->escape_string($info["latitude"])."', "
               . "longitute = '".$dbObject->escape_string($info["longitute"])."',"
               . "dayToken = '".$dbObject->escape_string($info["dayToken"])."', "
               . "`desc` = '".$dbObject->escape_string($info["desc"])."', "
               . "nombreBar = '".$dbObject->escape_string($info["nombreBar"])."', "
               . "songsAllowed = '".$dbObject->escape_string($info["songsAllowed"])."', "
               . "superClient = '".$dbObject->escape_string(json_encode($info["superClient"]))."'"
         . " WHERE id = '".$dbObject->escape_string($barId)."'";
      }
      // print_r($sql);
       $res = $dbObject->executeTransaction($sql);
       return $res;
    }

   public function registerBar($barId,$info){

       $dbObject = new ClassMySqlDB();
         $sql = "INSERT INTO bar (id, "
                             . "pass, "
                             . "email, "
                             . "nombreBar ) "
           . "VALUES   ('".$dbObject->escape_string($barId)."', "
                    . "'".$dbObject->escape_string(md5($info["pass"]))."', "
                    . "'".$dbObject->escape_string($info["email"])."', "
                    . "'".$dbObject->escape_string($info["nombreBar"])."')";
      // print_r($sql);
       $res = $dbObject->executeTransaction($sql);
       return $res;
    }

    public function saveBar($barId,$info){

       $dbObject = new ClassMySqlDB();
       $sql = "UPDATE bar SET "
               . "representante = '".$dbObject->escape_string($info["representante"])."', "
               . "telefono = '".$dbObject->escape_string($info["telefono"])."', "
               . "email = '".$dbObject->escape_string($info["email"])."', "
               . "latitude = '".$dbObject->escape_string($info["latitude"])."', "
               . "longitute = '".$dbObject->escape_string($info["longitute"])."',"
               . "dayToken = '".$dbObject->escape_string($info["dayToken"])."', "
               . "`desc` = '".$dbObject->escape_string($info["desc"])."', "
               . "nombreBar = '".$dbObject->escape_string($info["nombreBar"])."', "
               . "songsAllowed = '".$dbObject->escape_string($info["songsAllowed"])."', "
               . "available_yn = '".$dbObject->escape_string($info["available_yn"])."', "
               . "superClient = '".$dbObject->escape_string(json_encode($info["superClient"]))."'"
         . " WHERE id = '".$dbObject->escape_string($barId)."'";

       $res = $dbObject->executeTransaction($sql);
       return $res;
    }

    public function getBarConfig4Cat($catId){

        $dbObject = new ClassMySqlDB();
        $sql="select b.id,
                    b.latitude as lat,
                    b.longitute as lon,
                    b.songsAllowed,
                    b.superClient
             from bar b, catalog c
             where b.id = c.bar_id
             and c.idcatalog = '".$dbObject->escape_string($catId)."'
             limit 1";
       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }
    // --
    public function getBarPass($barId){

        $dbObject = new ClassMySqlDB();
        $sql="select pass
             from bar
             where id = '".$dbObject->escape_string($barId)."'";
       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }

    public function setBarPass($barId,$newPass){
        $dbObject = new ClassMySqlDB();
        $sql="update bar
             set pass = '".$dbObject->escape_string(md5($newPass))."'
             where id = '".$dbObject->escape_string($barId)."'";
       $res = $dbObject->executeTransaction($sql);
       if($res > 0)
           return true;
       else
           return false;
    }

    public function sumCredits2Bar($barId,$credits){
        $dbObject = new ClassMySqlDB();
        $sql="update bar
             set credits_loaded = credits_loaded + ".$dbObject->escape_string($credits)."
             where id = '".$dbObject->escape_string($barId)."'";
       $res = $dbObject->executeTransaction($sql);
       if($res > 0)
           return true;
       else
           return false;
    }
}
