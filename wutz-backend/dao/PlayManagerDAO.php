<?php
include_once  $_SESSION["ROOT_PATH"].'/common/ClassMySqlDB.php';
include_once  $_SESSION["ROOT_PATH"].'/common/Utils.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlayManagerDAO
 *
 * @author cristian toledo
 */
class PlayManagerDAO
{

    public function getSongInfo($catId,$songId)  {
       $dbObject = new ClassMySqlDB();
       $sql = "select   s.songid,
                        s.name,
                        s.file_path,
                        s.file_name,
                        ar.name as artist,
                        al.name as album,
                        s.pic
             from song s left join album al
                      on s.album_idalbum = al.idalbum
             left join artist ar
             on  al.artist_idartist = ar.idartist
             where s.songid = '".$dbObject->escape_string($catId)."' "
           ."and ar.idcatalog = '".$dbObject->escape_string($catId)."'";

       $res = $dbObject->getArrayFromQuery($sql);
       if(sizeof($res) > 0){
           return $res[0];
       }
       return false;
    }

    public function getAllCatalog($catId) {
       $dbObject = new ClassMySqlDB();
       $sql = "select   s.songid,
                        s.name,
                        s.file_path,
                        s.file_name,
                        ar.name as artist,
                        al.name as album
             from song s left join album al
                      on s.album_idalbum = al.idalbum
             left join artist ar
             on  al.artist_idartist = ar.idartist "
          ." where ar.idcatalog = '".$dbObject->escape_string($catId)."'";

       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }

    public function getPlayListSongs($catId) {
       $dbObject = new ClassMySqlDB();
       $sql = "select   s.songid,
                        s.name,
                        s.file_path,
                        s.file_name,
                        s.pic,
                        s.media_type,
                        s.extension,
                        ar.name as artist,
                        al.name as album,
                        al.idalbum,
                        ar.idartist,
                        gal.lfm_json as album_info,
                        pl.client_guid
               from song s
                     left join album al
                     on s.album_idalbum = al.idalbum and s.idcatalog = al.idcatalog
                     left join artist ar
                     on  al.artist_idartist = ar.idartist and al.idcatalog = ar.idcatalog
                     left join gen_album gal
                     on (gal.nameid = replace(lower(al.name),' ','') and  gal.art_nameid = replace(lower(ar.name),' ',''))
                     join playlist pl
                     on s.songId = pl.song_songid and s.idcatalog = pl.idcatalog
                     and pl.status='new'
                     and pl.idcatalog = '".$dbObject->escape_string($catId)."'
                order by pl.reqid asc";

       $res = $dbObject->getArrayFromQuery($sql);

       $dbObject = new ClassMySqlDB();
       $sql = "update playlist set status='queued'"
               . "where idcatalog = '".$dbObject->escape_string($catId)."'";
       $dbObject->executeTransaction($sql);

       return $res;
    }

    public function getPlayListSongsReadOnly($catId) {
       $dbObject = new ClassMySqlDB();
       $sql = "select   s.name,
                        s.pic,
                        s.media_type,
                        s.extension,
                        s.songid,
                        al.idcatalog,
                        al.idalbum,
                        ar.idartist,
                        ar.name as artist,
                        al.name as album,
                        gal.lfm_json as album_info,
                        pl.client_guid
               from song s
                     left join album al
                     on s.album_idalbum = al.idalbum and s.idcatalog = al.idcatalog
                     left join artist ar
                     on  al.artist_idartist = ar.idartist and al.idcatalog = ar.idcatalog
                     left join gen_album gal
                     on (gal.nameid = replace(lower(al.name),' ','') and  gal.art_nameid = replace(lower(ar.name),' ',''))
                     join playlist pl
                     on s.songId = pl.song_songid and s.idcatalog = pl.idcatalog
                     and pl.idcatalog = '".$dbObject->escape_string($catId)."'
                order by pl.reqid asc";

       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }

    public function isSongInPlaylist($catId,$songId){

        $dbObject = new ClassMySqlDB();
        $sql = "select count(1) as count from playlist where song_songid = '".$songId."' "
                . "and idcatalog = '".$dbObject->escape_string($catId)."'";
        $res = $dbObject->getArrayFromQuery($sql);
        if($res[0]["count"] > 0)
            return true;
        else
            return false;
    }

    public function removeSongFromPlayList($catId,$guid,$songid){
        $dbObject = new ClassMySqlDB();
        $sql = "DELETE
                FROM playlist
                WHERE song_songid = '".$dbObject->escape_string($songid)."'
                AND client_guid = '".$dbObject->escape_string($guid)."' "
             . "AND idcatalog = '".$dbObject->escape_string($catId)."'";
        $res = $dbObject->executeTransaction($sql);
        if($res > 0)
            return true;
        else
            return false;
    }

    public function cleanCatalogPlayList($catId){
        $dbObject = new ClassMySqlDB();
        $sql = "DELETE
                FROM client
                WHERE idcatalog = '".$dbObject->escape_string($catId)."'";
        $res = $dbObject->executeTransaction($sql);
        if($res > 0)
            return true;
        else
            return false;
    }

    public function getRandomSongId($catId)    {
        $dbObject = new ClassMySqlDB();
        $sql = "select songid from song "
              ."where idcatalog = '".$dbObject->escape_string($catId)."'";

        $res = $dbObject->getArrayFromQuery($sql);

      //  print_r($res);
        $index = array_rand($res, 1);

        //print_r($res);

        return $res[$index]["songid"];
    }

    public function getClient($catId, $guid){
       $dbObject = new ClassMySqlDB();
       $sql = "select * from client "
              . "where  "
              . " guid = '".$dbObject->escape_string($guid)."' "
              ." and idcatalog = '".$dbObject->escape_string($catId)."'";

       $res = $dbObject->getArrayFromQuery($sql);
       if(sizeof($res) > 0)
            return $res[0];
       else
            return false;
    }

    public function addClient($catId, $guid) {
       $dbObject = new ClassMySqlDB();
       $sql = "insert into client(idcatalog,guid,songs_in_queue) "
               . "values ('".$dbObject->escape_string($catId)."','".$dbObject->escape_string($guid)."',0)";

    //   print $sql;

       $res = $dbObject->executeTransaction($sql);
       if($res > 0)
           return true;
       else
           return false;

    }

    public function updateClient($catId, $client){
       $dbObject = new ClassMySqlDB();
       $sql = "update client
                set songs_in_queue = ".$client["songs_in_queue"].",
                    time_last_queue = CURRENT_TIME(),
                    credits = ".$client["credits"]."
                where  guid = '".$dbObject->escape_string($client["guid"])."' "
             ." and idcatalog = '".$dbObject->escape_string($catId)."'";

      // print($sql);

       $res = $dbObject->executeTransaction($sql);
       if($res > 0)
           return true;
       else
           return false;

    }

    public function addToPlayList($catId,$guid, $songId)   {
       $dbObject = new ClassMySqlDB();
       $sql = "insert into playlist(idcatalog, song_songid, client_guid) "
            . "values('$catId', $songId,'$guid')";

       //print_r($sql);
       $res = $dbObject->executeTransaction($sql);
       if($res > 0)
           return true;
       else
           return false;

    }


    public function resetPlayList($catId) {
       $dbObject = new ClassMySqlDB();
       $sql = "UPDATE playlist set status = 'new' "
               ." where idcatalog = '".$dbObject->escape_string($catId)."'";
       $res = $dbObject->executeTransaction($sql);
       if($res > 0)
           return true;
       else
           return false;

    }
}
