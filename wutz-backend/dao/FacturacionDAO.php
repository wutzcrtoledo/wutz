<?php
include_once  $_SESSION["ROOT_PATH"].'/common/ClassMySqlDB.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FacturacionDAO
 *
 * @author CRTOLEDO
 */
class FacturacionDAO {

    private $config;
    public function FacturacionDAO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }


    public function crearNuevasFacturasEsteMes(){
      $dbObject = new ClassMySqlDB();
      $sql = "INSERT into facturacion (`bar_id`,`credits`,`price_credits`,`plan`,`arriendo_equipo`,`fecha_vencimiento`, `total_facturado`)
              SELECT id as bar_id,
                     credits_loaded as credits,
                     precio_arriendo as arriendo_equipo,
                     precio_plan as plan,
                     price_credit as price_credit,
                     str_to_date(concat(month(CURRENT_TIMESTAMP)+1,'/',dia_venc ,'/',year(CURRENT_TIMESTAMP),' 12:00:00 AM'),  '%m/%e/%Y %h:%i:%s %p') as fecha_vencimiento,
                     (price_credit*credits_loaded) - ((price_credit*credits_loaded)*(100-perc_para_bar))/100 + (precio_arriendo + precio_plan)  as total_facturado
              FROM bar
              WHERE LOWER(status) in ('active')";
          $res = $dbObject->executeTransaction($sql);

          $sql =  "UPDATE bar set credits_loaded = 0
              where lower(status) in ('active')";
          $res = $dbObject->executeTransaction($sql);

          $sql = "UPDATE bar set status = 'active'
              where lower(status) in ('new')";

      $res = $dbObject->executeTransaction($sql);
      return $res;
    }

    public function aplicarInteresEnMora(){
      $dbObject = new ClassMySqlDB();
      $sql = "UPDATE `facturacion`
              SET  interes = (interes + (total_facturado*0.02))
              WHERE  date(CURRENT_TIMESTAMP) >= fecha_vencimiento and estado = 'pendiente'";

      $res = $dbObject->executeTransaction($sql);
      return $res;
    }
}
