<?php
include_once  $_SESSION["ROOT_PATH"].'/common/ClassMySqlDB.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GenCatalogDAO
 *
 * @author CRTOLEDO
 */
class GenCatalogDAO {
    private $config;
    public function GenCatalogDAO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }
    public function getGenArtistWithNoImages(){
       $dbObject = new ClassMySqlDB();
       $sql = "select * from gen_artist where lfm_img_url is null or lfm_img_url = ''";
       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }

    public function getAllGenArtists(){
       $dbObject = new ClassMySqlDB();
       $sql = "select * from gen_artist";
       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }

    public function setGenArtist($getArtist){
       $dbObject = new ClassMySqlDB();
       $sql = "update gen_artist
                set lfm_id = '".$getArtist["lfm_id"]."',
                    lfm_img_url = '".$getArtist["lfm_img_url"]."',
                    lfm_json = '".$dbObject->escape_string($getArtist["lfm_json"])."'
                where nameid = '".$getArtist["nameid"]."'";
       $res = $dbObject->executeTransaction($sql);
       return $res;
    }
    public function getGenAlbumsWithNoImages(){
       $dbObject = new ClassMySqlDB();
       $sql = "select ar.name as artist_name,
                        al.nameid,
                        al.name,
                        al.art_nameid
                 from gen_artist ar,
                      gen_album al
                 where al.art_nameid = ar.nameid
                 and (al.lfm_img_url is null or al.lfm_img_url in ('','-'))";
       $res = $dbObject->getArrayFromQuery($sql);
       return $res;
    }
    public function setGenAlbum($getAlbum){
       $dbObject = new ClassMySqlDB();
       $sql = "update gen_album
               set lfm_id = '".$getAlbum["lfm_id"]."',
                   lfm_img_url = '".$getAlbum["lfm_img_url"]."',
                   lfm_json = '".$dbObject->escape_string($getAlbum["lfm_json"])."'
                where art_nameid = '".$getAlbum["art_nameid"]."'"
               . "and nameid = '".$getAlbum["nameid"]."'";
       $res = $dbObject->executeTransaction($sql);
       return $res;
    }

    public function cleanUpRowsWithNoImage(){
       $dbObject = new ClassMySqlDB();
       $sql = "delete from gen_artist where lfm_img_url is null or lfm_img_url in ('','-')";
       $res = $dbObject->executeTransaction($sql);
       $sql = "delete from gen_album where lfm_img_url is null or lfm_img_url in ('','-')";
       $res = $dbObject->executeTransaction($sql);
       return $res;
    }

    public function pullLocalCatalog2GenCatalog($catId){

        $dbObject = new ClassMySqlDB();

        $sql = "INSERT IGNORE INTO gen_artist(nameid, name)
                select replace(lower(name),' ','') as nameid, name
                FROM artist where idcatalog = $catId";
        $dbObject->executeTransaction($sql);
        //------------------------------------------------------------
        $sql = "INSERT IGNORE INTO gen_album(art_nameid,nameid,name)
                select distinct
                       replace(lower(ar.name),' ','') as art_nameid,
                       replace(lower(al.name),' ','') as nameid,
                       al.name
                FROM artist ar,album al
                where
                al.artist_idartist = ar.idartist and
                ar.idcatalog = al.idcatalog and
                ar.idcatalog = $catId";
        $dbObject->executeTransaction($sql);
        //------------------------------------------------------------
        $sql = "INSERT IGNORE INTO gen_song(art_nameid,alb_nameid,nameid,name)
                select distinct
                       replace(lower(ar.name),' ','') as art_nameid,
                       replace(lower(al.name),' ','') as alb_nameid,
                       replace(lower(sg.name),' ','') as nameid,
                       sg.name
                FROM artist ar,album al,song sg
                where
                al.artist_idartist = ar.idartist and
                sg.album_idalbum = al.idalbum and
                ar.idcatalog = al.idcatalog and
                sg.idcatalog = al.idcatalog and
                ar.idcatalog = $catId";
        $dbObject->executeTransaction($sql);

    }

}
