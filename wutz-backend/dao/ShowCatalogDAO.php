<?php
include_once  $_SESSION["ROOT_PATH"].'/common/ClassMySqlDB.php';
include_once  $_SESSION["ROOT_PATH"].'/common/Utils.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShowCatalogDAO
 *
 * @author CRTOLEDO
 */
class ShowCatalogDAO {
    private $config;
    public function ShowCatalogDAO(){
        $configUrl = $_SESSION["ROOT_PATH"]."/json/config.json";
        $confCont = file_get_contents($configUrl);
        $this->config = json_decode($confCont, true);
    }
    public function getAllArtist($catId){
       $dbObject = new ClassMySqlDB();
      // $sql = "select idartist,name from artist where idcatalog = $catId order by name asc";
       $sql = "select a.idartist,
                        a.name,
                        ga.lfm_img_url
                 from artist a
                      left join gen_artist ga
                 on ga.nameid = replace(lower(a.name),' ','')
                 where a.idcatalog = $catId
                 order by a.name asc";
       return $dbObject->getArrayFromQuery($sql);
    }

    public function getArtistAlbumList($catId,$artistId){
       $dbObject = new ClassMySqlDB();
       //$sql = "select idalbum,name from album where artist_idartist = $artistId and idcatalog = $catId order by name asc";
       $sql = "select al.idalbum,
                     al.name,
                     ga.lfm_img_url
              from album al
                left join artist ar
                      on  al.artist_idartist = ar.idartist
                left join gen_album ga
                on (ga.nameid = replace(lower(al.name),' ','') and ga.art_nameid = replace(lower(ar.name),' ',''))
              where al.artist_idartist = $artistId
              and al.idcatalog = $catId
              order by al.name asc";

       return $dbObject->getArrayFromQuery($sql);
    }

    public function getAlbumSongList($catId, $albumId){
       $dbObject = new ClassMySqlDB();
       $sql = "select songid,name,extension,media_type from song where album_idalbum = $albumId and idcatalog = $catId order by track,name asc";

       return $dbObject->getArrayFromQuery($sql);
    }

    //---------------------------------------------------------------------------

    public function search4Albums($catId,$seTxt){
       $dbObject = new ClassMySqlDB();
       $sql = "select  al.idalbum,
                       al.name,
                       IFNULL(ga.lfm_img_url,'./img/logo.png') as lfm_img_url,
                       ar.name as artist,
                       ar.idartist
               from album al
                  left join artist ar
                    on  al.artist_idartist = ar.idartist
                  left join gen_album ga
                    on (ga.nameid = replace(lower(al.name),' ','') and ga.art_nameid = replace(lower(ar.name),' ',''))
               where ar.idcatalog = ".$dbObject->escape_string($catId)."
               and INSTR(LOWER(REPLACE(al.name,' ','')),'".$dbObject->escape_string($seTxt)."') > 0";
       return $dbObject->getArrayFromQuery($sql);
    }

    //---------------------------------------------------------------------------

    public function search4Songs($catId,$seTxt){
       $dbObject = new ClassMySqlDB();
       $sql = "select  s.songid,
                       s.name,
                       s.media_type,
                       ar.name as artist,
                       al.name as album,
                       ar.idartist,
                       al.idalbum
               from song s
                  left join album al
                    on s.album_idalbum = al.idalbum
                  left join artist ar
                    on  al.artist_idartist = ar.idartist
               where ar.idcatalog = ".$dbObject->escape_string($catId)."
               and INSTR(LOWER(REPLACE(s.name,' ','')),'".$dbObject->escape_string($seTxt)."') > 0";
       return $dbObject->getArrayFromQuery($sql);
    }

    //---------------------------------------------------------------------------

    public function getSongs4Artist($catId,$artId){
       $dbObject = new ClassMySqlDB();
       $sql = "select  s.songid,
                       s.name,
                       s.media_type,
                       ar.name as artist,
                       al.name as album,
                       ar.idartist,
                       al.idalbum
               from song s
                  left join album al
                    on s.album_idalbum = al.idalbum
                  left join artist ar
                    on  al.artist_idartist = ar.idartist
               where ar.idcatalog = ".$dbObject->escape_string($catId)."
               and ar.idartist = ".$dbObject->escape_string($artId)."";
       return $dbObject->getArrayFromQuery($sql);
    }

}
