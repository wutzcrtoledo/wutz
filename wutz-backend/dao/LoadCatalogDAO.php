<?php
include_once  $_SESSION["ROOT_PATH"].'/common/ClassMySqlDB.php';
include_once  $_SESSION["ROOT_PATH"].'/common/Utils.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoadCatalogDAO
 *
 * @author CRTOLEDO
 */
class LoadCatalogDAO {

    public function insertBulkArtist($catId,$artists)
    {
        $dbObject = new ClassMySqlDB();
        $affRows = 0;
        foreach($artists as $art)  {
            $artName = isset($art["name"])?$art["name"]:$art;
            $inserts[] = "('".$dbObject->escape_string($artName)."',"
                        . "'".$dbObject->escape_string($catId)."')";
        }
        $sql = "insert into artist(name,idcatalog) VALUES ".implode(",",$inserts);
        $affRows = $dbObject->executeTransaction($sql);
        if($affRows > 0)
        {
          $dbObject = new ClassMySqlDB();
          $sql = "select * from artist where idcatalog = '".$dbObject->escape_string($catId)."'";
          return $dbObject->getArrayFromQuery($sql);
          //return true;
        }

        return false;
    }

    public function insertBulkAlbums($catId, $albums)
    {
        $dbObject = new ClassMySqlDB();
        $affRows = 0;

       // print_r($albums);

        foreach($albums as $alb)        {
           //$albName = str_ireplace($alb["artName"],"",$alb["albumname"]);
            
           $inserts[] = "('".$dbObject->escape_string($alb["albumname"])."',"
                           .$dbObject->escape_string($alb["idartist"]).","
                           . "".$dbObject->escape_string($catId).")";
        }
        $sql = "insert into album(name,artist_idartist,idcatalog) VALUES ".implode(",",$inserts);
        $affRows = $dbObject->executeTransaction($sql);
        if($affRows > 0)
        {
          $dbObject = new ClassMySqlDB();
          $sql = "select al.idalbum,al.name as name,ar.idartist, ar.name as artistName
                    from album al, artist ar
                    where al.artist_idartist = ar.idartist
                    and ar.idcatalog =  al.idcatalog
                    and ar.idcatalog = '".$dbObject->escape_string($catId)."'";
          return $dbObject->getArrayFromQuery($sql);
         }

        return false;
    }

    public function getCatalogArtist($catId){
          $dbObject = new ClassMySqlDB();
          $sql = "select * from artist where idcatalog = '".$dbObject->escape_string($catId)."'";
          return $dbObject->getArrayFromQuery($sql);
    }

    public function getCatalogAlbums($catId)
    {
          $dbObject = new ClassMySqlDB();
          $sql = "select al.idalbum,al.name as name,ar.idartist, ar.name as artistName
                    from album al, artist ar
                    where al.artist_idartist = ar.idartist
                    and ar.idcatalog =  al.idcatalog
                    and ar.idcatalog = '".$dbObject->escape_string($catId)."'";
          return $dbObject->getArrayFromQuery($sql);
    }

    public function insertBulkSongs($catId, $songs) {
        $dbObject = new ClassMySqlDB();
        $affRows = 0;
        $utils = new Utils();

          //      print_r($songs);


        foreach($songs as $song)  {
           $inserts[] = "('".$dbObject->escape_string($song["songName"])."',"
                        ."'".$dbObject->escape_string($song["pic"])."',"
                        ."'".$dbObject->escape_string($song["songPath"])."',"
                        ."'".$dbObject->escape_string($song["songFileName"])."',"
                        ."'".$dbObject->escape_string($song["track"])."',"
                        ."'".$dbObject->escape_string($song["mediaType"])."',"
                        ."'".$dbObject->escape_string($song["extension"])."',"
                            .$dbObject->escape_string($song["idalbum"]).","
                        . "".$dbObject->escape_string($catId).")";
        }
        $sql = "INSERT INTO song(name,pic, file_path, file_name,track,media_type,extension, album_idalbum, idcatalog) VALUES ".implode(",",$inserts);
        $affRows = $dbObject->executeTransaction($sql);
        if($affRows > 0)
        {
          //$dbObject = new ClassMySqlDB();
          //$sql = "select * from song where idcatalog = '".$dbObject->escape_string($catId)."'";
          //return $dbObject->getArrayFromQuery($sql);
          return true;
        }
        else {
             return false;
        }

      return $affRows>0;
    }


    function removeAllArtists(){
        $dbObject = new ClassMySqlDB();
        $sql = "delete from artist";
        $affRows = $dbObject->executeTransaction($sql);
        if($affRows > 0)
        {
            return true;
        }
        else
            return false;
    }

    function createGetCatalog($barId){

        $dbObject = new ClassMySqlDB();
        $barId = $dbObject->escape_string($barId);
        $catName = $dbObject->escape_string($barId." s Catalog 1");
        $sql = "select * from catalog where bar_id = '$barId'";
        $res = $dbObject->getArrayFromQuery($sql);
        if(sizeof($res)>0){
            $res = array("status"=>"EXIST","cats"=>$res);
            return $res;
        }
        else{
             $columns = "('".$catName."',"
                        ."'',"
                        ."'".$barId."')";
           $sql = "INSERT INTO catalog(name,path, bar_id) VALUES ".$columns;
           $affRows = $dbObject->executeTransaction($sql);
           $sql = "select * from catalog where bar_id = '$barId'";
           $res = $dbObject->getArrayFromQuery($sql);
           $res = array("status"=>"NEW","cats"=>$res);
        }
        return $res;
    }

    public function getOrInsertBulkArtist($catId, $artistList){
        $utils = new Utils();
        $dbObject = new ClassMySqlDB();
        $implodedList = $utils->implodeArrForSQL(array_column($artistList, "name"));
        $implodedList = strtolower(str_replace(" ", "", $implodedList));
        $sql = "select * from artist
                where idcatalog = $catId
                and replace(lower(name),' ','') in (".$implodedList.")";
        error_log($sql."\n\n");
        $foundArr = $dbObject->getArrayFromQuery($sql);

        $art2Insert = array();
        foreach ($artistList as $idx => $artNode) {


            if(in_array($artNode["name"], array_column($foundArr,"name"))){
                //array_push($art2Insert, $artNode);
                unset($artistList[$idx]);
            }
        }

        error_log("Arr2 Insert : ".  json_encode($artistList));
        $createdArts = array();
        if(sizeof($artistList) > 0){
          $createdArts = $this->insertBulkArtist($catId, $artistList);
        }
        return array_merge($foundArr,$createdArts);
    }

    public function getOrInsertBulkAlbums($catId,$albums){
        $dbObject = new ClassMySqlDB();
        $utils = new Utils();
        $implodedList = $utils->implodeArrForSQL(array_column($albums, "albumname"));
        $implodedList = strtolower(str_replace(" ", "", $implodedList));
        $sql = "select al.idalbum,al.name as name,ar.idartist, ar.name as artistName
                from album al, artist ar
                where al.artist_idartist = ar.idartist
                and ar.idcatalog =  al.idcatalog
                and ar.idcatalog = $catId
                and replace(lower(al.name),' ','') in (".$implodedList.")";
        $foundArr = $dbObject->getArrayFromQuery($sql);
        $albumsNames = array_diff(array_column($albums, "albumname"),   array_column($foundArr,"name"));
        $alb2Insert = array();
        foreach ($albums as $albNode) {
            if(in_array($albNode["albumname"], $albumsNames)){
                array_push($alb2Insert, $albNode);
            }
        }
        $createdAlbms = array();
        if(sizeof($alb2Insert) > 0){
            $createdAlbms = $this->insertBulkAlbums($catId, $alb2Insert);
        }
        return array_merge($foundArr,$createdAlbms);
    }

    public function insertBulkSongsIfExist($catId,$songs){
        $dbObject = new ClassMySqlDB();
        $utils = new Utils();
        $implodedList = $utils->implodeArrForSQL(array_column($songs, "songName"));
        $sql = "select * from song
                where idcatalog = $catId
                and name in (".$implodedList.")";
        $foundArr = $dbObject->getArrayFromQuery($sql);
        $notExistedSongNames = array_diff(array_column($songs, "songName"),   array_column($foundArr,"name"));
        $son2Insert = array();
        foreach ($songs as $sNode) {
            if(in_array($sNode["songName"], $notExistedSongNames)){
                array_push($son2Insert, $sNode);
            }
        }
        if(sizeof($son2Insert) > 0){
            return  $this->insertBulkSongs($catId, $son2Insert);
        }
        return true;
    }

    function removeCatalog($catalogId){

        $dbObject = new ClassMySqlDB();
        $dbObject->escape_string($catalogId);
        $sql = "delete from catalog where idcatalog = '$catalogId'";
        $affRows = $dbObject->executeTransaction($sql);
        if($affRows > 0)
        {
            return true;
        }
        else
            return false;
    }

    function setBarStatus($barId,$status){
        $dbObject = new ClassMySqlDB();
        $sql = "update bar set available_yn='$status' where id = '".$dbObject->escape_string($barId)."'";
        $affRows = $dbObject->executeTransaction($sql);
        if($affRows > 0)
        {
            return true;
        }
        else
            return false;

    }
}
