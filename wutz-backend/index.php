<?php
include_once './common/setRoot.php';
if (!function_exists('getallheaders'))  {
    function getallheaders()
    {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}
//$uri = $_SERVER['HEADER'];
//print "We are on Index";
$endpoint = $_REQUEST["endpoint"];
if (preg_match("/^login$/i", $endpoint, $match)){
  //  print  "is login";
    include_once('./delegate/wzDelServ2Serv.php');
    $objIns = new WzDelServ2Serv();
    $objIns->dispatchRequest(getallheaders(),'auth',$_REQUEST);
}
else if(preg_match("/^getBar\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
    include_once('./delegate/wzDelServ2Serv.php');
    $objIns = new WzDelServ2Serv();
    $request["barId"] = $match[1];
    $objIns->dispatchRequest(getallheaders(),'getBarDetails',$request);
    //include('delegate/wutzDelegMan.php?fnc=getBarDetails&barId='.$match[1]);
}
else if (preg_match("/^refresh$/i", $endpoint, $match)){
    //  RewriteRule ^refresh$   delegate/wzDelServ2Serv.php?fnc=refresh [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'refresh',$_REQUEST);
}
else if (preg_match("/^cleanCatalog$/i", $endpoint, $match)){
    //RewriteRule ^cleanCatalog   delegate/wzDelServ2Serv.php?fnc=cleanCatalog [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'cleanCatalog',$_REQUEST);
}
else if (preg_match("/^getFullCatalog$/i", $endpoint, $match)){
    //  RewriteRule ^getFullCatalog   delegate/wzDelServ2Serv.php?fnc=getFullCatalog  [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'getFullCatalog',$_REQUEST);
}
else if (preg_match("/^unqueueSong$/i", $endpoint, $match)){
//RewriteRule ^unqueueSong   delegate/wzDelServ2Serv.php?fnc=unqueueSong  [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'unqueueSong',$_REQUEST);
}
else if (preg_match("/^addSongToQueue$/i", $endpoint, $match)){
//RewriteRule ^addSongToQueue   delegate/wzDelServ2Serv.php?fnc=addSongToQueue  [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'addSongToQueue',$_REQUEST);
}
else if (preg_match("/^transferCredits$/i", $endpoint, $match)){
    //RewriteRule ^transferCredits   delegate/wzDelServ2Serv.php?fnc=transferCredits [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'transferCredits',$_REQUEST);
}
else if (preg_match("/^refresh$/i", $endpoint, $match)){
    //  RewriteRule ^refresh$   delegate/wzDelServ2Serv.php?fnc=refresh [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'refresh',$_REQUEST);
}
else if (preg_match("/^createTransfCode$/i", $endpoint, $match)){
   //RewriteRule ^createTransfCode   delegate/wzDelServ2Serv.php?fnc=createTransfCode [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'createTransfCode',$_REQUEST);
}
else if (preg_match("/^addCredits$/i", $endpoint, $match)){
    //  RewriteRule ^addCredits   delegate/wzDelServ2Serv.php?fnc=addCredits [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'addCredits',$_REQUEST);
}
else if (preg_match("/^sendPassRecoverPassCode$/i", $endpoint, $match)){
    //  RewriteRule ^sendPassRecoverPassCode   delegate/wzDelServ2Serv.php?fnc=sendPassRecoverPassCode [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $objIns->dispatchRequest(getallheaders(),'sendPassRecoverPassCode',$_REQUEST);
}
else if (preg_match("/^getUpdatesVersion\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
    //  RewriteRule ^getUpdatesVersion/([a-zA-Z0-9_]*)$   delegate/wzDelServ2Serv.php?fnc=getUpdatesVersion&sys=$1 [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $_REQUEST["sys"] = $match[1];
      $objIns->dispatchRequest(getallheaders(),'getUpdatesVersion',$_REQUEST);
}
else if (preg_match("/^requestRecoverPassCode\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
    //  RewriteRule ^$   delegate/wzDelServ2Serv.php?fnc=requestRecoverPassCode&barId=$1 [nc,qsa]
      include_once('./delegate/wzDelServ2Serv.php');
      $objIns = new WzDelServ2Serv();
      $_REQUEST["barId"] = $match[1];
      $objIns->dispatchRequest(getallheaders(),'requestRecoverPassCode',$_REQUEST);
}
//---------------------------------------------------------------------------------------------------
else if(preg_match("/^searchOnCatalog\/([a-zA-Z0-9]+)\/([a-zA-Z0-9]+)$/i", $endpoint, $match)){
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["barId"] = $match[1];
    $_REQUEST["keyTxt"] = $match[2];
    $objIns->dispatchRequest(getallheaders(),'searchOnCatalog',$_REQUEST);    
}
else if(preg_match("/^checkCredits$/i", $endpoint, $match)){
    //RewriteRule ^checkCredits   delegate/wutzDelegMan.php?fnc=checkCredits [nc,qsa]
     include_once('./delegate/wutzDelegMan.php');
     $objIns = new WutzDelegMan();
     $objIns->dispatchRequest(getallheaders(),'checkCredits',$_REQUEST);    
 }
 else if(preg_match("/^connect2Bar$/i", $endpoint, $match)){
//    RewriteRule ^connect2Bar   delegate/wutzDelegMan.php?fnc=connect2Bar [nc,qsa]
     include_once('./delegate/wutzDelegMan.php');
     $objIns = new WutzDelegMan();
     $objIns->dispatchRequest(getallheaders(),'connect2Bar',$_REQUEST);    
 }
 else if(preg_match("/^getCurrentPlayList$/i", $endpoint, $match)){
//RewriteRule ^getCurrentPlayList   delegate/wutzDelegMan.php?fnc=getPlayListSongs [nc,qsa]
     include_once('./delegate/wutzDelegMan.php');
     $objIns = new WutzDelegMan();
     $objIns->dispatchRequest(getallheaders(),'getPlayListSongs',$_REQUEST);    
 }
else if(preg_match("/^checkToken$/i", $endpoint, $match)){
   //RewriteRule ^checkToken   delegate/wutzDelegMan.php?fnc=checkToken [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $objIns->dispatchRequest(getallheaders(),'checkToken',$_REQUEST);    
}
else if(preg_match("/^addSongToQueue$/i", $endpoint, $match)){
   //RewriteRule ^addSongToQueue   delegate/wutzDelegMan.php?fnc=addSongToQueue [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $objIns->dispatchRequest(getallheaders(),'addSongToQueue',$_REQUEST);    
}
else if(preg_match("/^getArtistSongs\/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)$/i", $endpoint, $match)){
   //RewriteRule ^getArtistSongs/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)$   delegate/wutzDelegMan.php?fnc=getArtistSongs&catId=$1&artId=$2 [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["catId"] = $match[1];
    $_REQUEST["artId"] = $match[2];
    $objIns->dispatchRequest(getallheaders(),'getArtistSongs',$_REQUEST);    
}
else if(preg_match("/^getSongsPerAlbum\/([^/]+)/([^/]+)$/i", $endpoint, $match)){
//RewriteRule ^getSongsPerAlbum/([^/]+)/([^/]+)$   delegate/wutzDelegMan.php?fnc=getSongsPerAlbum&albId=$2&catId=$1 [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["catId"] = $match[1];
    $_REQUEST["albId"] = $match[2];
    $objIns->dispatchRequest(getallheaders(),'getSongsPerAlbum',$_REQUEST);    
}
else if(preg_match("/^getAlbumPerArtist\/([^/]+)/([^/]+)$/i", $endpoint, $match)){
//RewriteRule ^getAlbumPerArtist/([^/]+)/([^/]+)$   delegate/wutzDelegMan.php?fnc=getAlbumPerArtist&artId=$2&catId=$1 [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["catId"] = $match[1];
    $_REQUEST["artId"] = $match[2];
    $objIns->dispatchRequest(getallheaders(),'getAlbumPerArtist',$_REQUEST);    
}
else if(preg_match("/^getArtistList\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
//RewriteRule ^getArtistList/([a-zA-Z0-9_]*)$   delegate/wutzDelegMan.php?fnc=getArtistList&catId=$1 [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["catId"] = $match[1];    
    $objIns->dispatchRequest(getallheaders(),'getArtistList',$_REQUEST);    
}

else if(preg_match("/^getNearByBars\/([^/]+)/([^/]+)$/i", $endpoint, $match)){
//RewriteRule ^getNearByBars/([^/]+)/([^/]+)$   delegate/wutzDelegMan.php?fnc=getNearByBars&lat=$1&lon=$2 [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["lat"] = $match[1];
    $_REQUEST["lon"] = $match[2];
    $objIns->dispatchRequest(getallheaders(),'getNearByBars',$_REQUEST);    
}
else if(preg_match("/^isBarAvailable\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
   //RewriteRule ^isBarAvailable/([a-zA-Z0-9_]*)$   delegate/wutzDelegMan.php?fnc=getBarDetails&barId=$1&checkAvailable=true [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["barId"] = $match[1];
    $_REQUEST["checkAvailable"] = "true";
    $objIns->dispatchRequest(getallheaders(),'getBarDetails',$_REQUEST);    
}
else if(preg_match("/^searchBar\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
//RewriteRule ^searchBar/([a-zA-Z0-9_]*)$   delegate/wutzDelegMan.php?fnc=getBar&barId=$1 [nc,qsa]
    include_once('./delegate/wutzDelegMan.php');
    $objIns = new WutzDelegMan();
    $_REQUEST["barId"] = $match[1];    
    $objIns->dispatchRequest(getallheaders(),'getBar',$_REQUEST);    
}

else if (preg_match("/^updateLocalCatalog\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
    // RewriteRule ^updateLocalCatalog/([a-zA-Z0-9_]*)$   delegate/updateLocalCatalog.php?catalogId=$1 [nc,qsa]
      $_REQUEST["catalogId"] = $match[1];
      include('./delegate/updateLocalCatalog.php');
}
else if (preg_match("/^uploadLocalCatalog$/i", $endpoint, $match)){
    //  RewriteRule ^uploadLocalCatalog$   delegate/uploadLocalCatalog.php [nc,qsa]
    include('./delegate/uploadLocalCatalog.php');
}
else if (preg_match("/^registerBar$/i", $endpoint, $match)){
    //  RewriteRule ^registerBar$   delegate/registerBar.php [nc,qsa]
    include('./delegate/registerBar.php');
}
else if (preg_match("/^pullCat2Gen\/([a-zA-Z0-9_]*)$/i", $endpoint, $match)){
    //  RewriteRule ^pullCat2Gen/([a-zA-Z0-9_]*)$   delegate/pullCat2Gen.php?catalogId=$1 [nc,qsa]
    $_REQUEST["catalogId"] = $match[1];
    include('./delegate/pullCat2Gen.php');
}
else if (preg_match("/^uploadLocalServerInfo$/i", $endpoint, $match)){
    //RewriteRule ^uploadLocalServerInfo$   delegate/uploadLocalServerInfo.php [nc,qsa]
    include('./delegate/uploadLocalServerInfo.php');
}