<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once $argv[1];
include_once $_SESSION["ROOT_PATH"].'/bo/GenCatalogBO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';
echo "Process Starting\n";
$bo = new GenCatalogBO();
if($bo->refreshGetCatalogFromLasfFM()){
     requestRefreshCats2Apps();
     print("refreshGetCatalogFromLasfFM Process Finished DONE\n");
}
else{
     print("refreshGetCatalogFromLasfFM Process Failed\n");
}

function requestRefreshCats2Apps(){
       $catAdminBo = new WutzAdminBO();
       $initPath = $_SESSION["ROOT_PATH"]."/cache";
       $it = new RecursiveDirectoryIterator($initPath);
       foreach(new RecursiveIteratorIterator($it) as $file) {
           $fileName = $file->getFileName();
           $fullFilePath = $file->getPath();
           if($fileName !== "." && $fileName !== ".."){
                if (preg_match("/version_cat_[0-9]+\.txt/i", $fileName)){
                    print $fileName."\n";
                    unlink($fullFilePath."/".$fileName);
                    $catId = preg_replace('/version_cat_|\.txt/i',"", trim($fileName));
                    $catAdminBo->removeCatalogFromCache($catId);
                }
           }
       }
}
