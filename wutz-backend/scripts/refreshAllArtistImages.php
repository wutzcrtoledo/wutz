<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once $argv[1];
include_once $_SESSION["ROOT_PATH"].'/bo/GenCatalogBO.php';
include_once $_SESSION["ROOT_PATH"].'/bo/WutzAdminBO.php';
echo "Process Starting\n";
$bo = new GenCatalogBO();
if($bo->refreshAllArtistImgFromLasfFM()){
     removeAllArtistFromCache();
     print("refreshAllArtistImages Process Finished DONE\n");
}
else{
     print("refreshAllArtistImages Process Failed\n");
}

function removeAllArtistFromCache(){
       $catAdminBo = new WutzAdminBO();
       $initPath = $_SESSION["ROOT_PATH"]."/cache";
       $it = new RecursiveDirectoryIterator($initPath);
       foreach(new RecursiveIteratorIterator($it) as $file) {
           $fileName = $file->getFileName();
           $fullFilePath = $file->getPath();
           if($fileName !== "." && $fileName !== ".."){
                if (preg_match("/artists_[0-9]+\.json/i", $fileName)){
                    print $fileName."\n";
                    unlink($fullFilePath."/".$fileName);
                    $catId = preg_replace('/artists_|\.json/i',"", trim($fileName));
                    //$catAdminBo->removeCatalogFromCache($catId);
                }
           }
       }
}
