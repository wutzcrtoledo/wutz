var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var cors = require("cors");
var logger = require(__dirname+"/lib/common/logging")();
var edgescapeParser = require('edgescape-parser');
const fs = require("fs");

var conf =  JSON.parse(fs.readFileSync("./config/default.json"));
var app = module.exports = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(edgescapeParser());

//Configure Sessions in MongoDb
let session = require('express-session');
//let mongo = require('mongoose');

let mongo = require(__dirname+"/lib/dao/MongoDAO");

app.use(cookieParser());
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  }));
/**
app.use(session({
    secret: 'sercretrightveryscret',
    maxAge: new Date(Date.now() + 3600000),
    store: mongo.getDbSync(),
    resave: true,
    saveUninitialized: true
}));
**/


try {
    // Load all routes
    require('./routes')(app, logger);
} catch (e) {
    logger.error(e.message);
}