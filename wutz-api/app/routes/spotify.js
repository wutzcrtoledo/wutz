var bodyParser = require('body-parser');
let spoBO = require("../lib/bo/SpotifyBO");
let mainSess = require("../lib/common/Session");

module.exports = (app, logger) => {	
	app.get("/api/spo-callback", (req, res) => {
        //logger.info(req);
        console.log(req.query.code);
        spoBO.addLoginToken(req.query.code, _res => {     
           // req.session.user = _res;
            mainSess.startSession(req.session);
            mainSess.set("user",_res);
            res.json(_res);
        });        
    });
    
    app.get('/api/spo-login', function(req, res) {
        let spoConf = spoBO.getConfig();
        var scopes = spoConf.scopes;
        let my_client_id = spoConf.clientId;
        let redirect_uri = spoConf.redirectUri;
        console.log(my_client_id,redirect_uri);
        res.redirect('https://accounts.spotify.com/authorize' +
          '?response_type=code' +
          '&client_id=' + my_client_id +
          '&state=LOGIN' + 
          (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
          '&redirect_uri=' + encodeURIComponent(redirect_uri));
    });

    app.get('/api/spo-get-init-comp', (req, res) => {
        let tokauth = req.get('Authorization');
        console.log(tokauth);
        spoBO.initUserComponents(tokauth, _res => {
            res.json(_res);
        });        
    });

    app.post('/api/spo-select-playlist', (req, res) => {
        let tokauth = req.get('Authorization');
        spoBO.selectPlaylistForCloud(req.body.playlist,req.body.device, tokauth,_res => {
            res.json(_res);
        });        
    });

    

    
};


