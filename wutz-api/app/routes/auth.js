var bodyParser = require('body-parser');
let spoBO = require("../lib/bo/SpotifyBO");
let mainSess = require("../lib/common/Session");

module.exports = (app, logger) => {	
	    
    app.post('/api/login', function(req, res) {
        let user = req.body;
        mainSess.startSession(req.session);
        mainSess.set("user",user);
        spoBO.initUserComponents(user.session.access_token, _comp => {
            res.json(_comp);
        });
    });
};


