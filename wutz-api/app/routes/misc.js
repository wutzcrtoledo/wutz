var bodyParser = require('body-parser');
let emailBO = require("../lib/bo/EmailBO");

module.exports = (app, logger) => {
	app.use(function(req, res, next) {
		console.log("I'm setting the headers");
		//console.log(conf);
		res.header("Access-Control-Allow-Origin", "http://localhost:3000");
		res.header("Access-Control-Allow-Origin", "https://berserker.bluezinc.cl");
		//res.header("Access-Control-Allow-Origin", conf.clientAllowed);		
		res.header("Access-Control-Allow-Credentials", true);			
		res.header("Access-Control-Allow-Headers", "X-Requested-With, authorization,content-type");
		res.header("Access-Control-Allow-Methods", "GET,HEAD,PUT,PATCH,POST,DELETE");
		next();
	});
	
	app.post("/api/helloPost", function(req, res) {
		logger.info("Hello POST OK");
		res.json({"helloPostWorld":true});
	});

	app.get("/api/helloGet", function(req, res) {
		logger.info("Hello GET OK");
		res.json({"helloGetWorld":true});
	});

	app.post("/api/sendMail", function(req, res) {
		logger.info("Sending Email");
		emailBO.sendEmail(req.body,_res => {
			res.json(_res);
		});
	});
	
		
};
