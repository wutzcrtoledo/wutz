const fs = require("fs");
let sptfyApi = require("../dao/SpotifyDAO");
let usrDao = require("../dao/UserDAO");
let catBo = require("../bo/CatalogBO");
const _ = require("underscore");

let loggedUsers = {};

let spotifyConfig = JSON.parse(fs.readFileSync(__dirname+'/../../../config/default.json'));
spotifyConfig = spotifyConfig.spotify;

sptfyApi.init(spotifyConfig);


const addLoginToken = (_token, callback) => {
  //  spotifyApi.setAccessToken(_token);
  sptfyApi.getUsrAccessToken(_token, _user => {
    if(_user.success){
      usrDao.upsertUser(_user.user, _usr => {
         console.log(_usr);
          callback(_user.user);    
      });
    }
        
  });
    
};

const initUserComponents = (sess, callback) => {
  let retObj = {};
  sptfyApi.api.getAllUserPlaylist(sess, _playLists => {
    console.log(_playLists);
     if(_playLists.error){
        callback(_playLists.error);
        return ;
     }
      retObj.playlists = _playLists.items;
      sptfyApi.api.getDevices(sess, _devs => {
          console.log(_devs);
          retObj.devices = [];
          if(_devs.devices && _devs.devices.length > 0){
              retObj.devices = _devs.devices;
              _devs.devices.forEach(_device => {
                  if(_.filter(_playLists.items,{name: _device.name+"-Spotcloud-Playlist"}).length <= 0){
                      sptfyApi.api.createSpotcloudPlaylist(_device.name, sess);
                  }
              });          
          }

          retObj.playlists = _.filter(retObj.playlists, _node => {
                let ret = true;
                ret = ret && _node.owner.id !== "spotify";
                ret = ret && !/-Spotcloud-Playlist/ig.test(_node.name);
                return ret;
             
          });
          catBo.getBarForUser( _bar =>{
            retObj.bar = _bar;
            catBo.getCatalogsForBar(_cats => {
              retObj.catalogs = _cats;
              callback(retObj);
            });
          });
      });
  });
};

const selectPlaylistForCloud = (playListObj,device, sess, callback) => {



  sptfyApi.api.getTracksPerPlayList(sess, playListObj.id, _res => {
     callback(_res);
  });
};


const getConfig = () => {
    return spotifyConfig;
};




module.exports =  {
    addLoginToken: addLoginToken,
    getConfig: getConfig,
    initUserComponents:initUserComponents,
    selectPlaylistForCloud:selectPlaylistForCloud
};