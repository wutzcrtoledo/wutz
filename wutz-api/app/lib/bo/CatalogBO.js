const fs = require("fs");
let catDao = require("../dao/CatalogDAO");
const _ = require("underscore");
let session = require("../common/Session");

let boObj = {};

boObj.getBarForUser = (callback) => {
    let usr = session.get("user");
    catDao.getBarEventForUser(usr, _res => {
        _res = _.pick(_res,"_id","display_name","dayToken","desc","coords","representante","songsAllowed","telefono")
        session.set("bar",_res);
        if(callback)
            callback(_res);
    });
};

boObj.getCatalogsForBar = (callback) => {
    let bar = session.get("bar");
    catDao.getCatalogsForBar(bar, _res => {
        //_res = _.pick(_res,"_id","display_name","dayToken","desc","coords","representante","songsAllowed","telefono")
        session.set("catalogs",_res);
        if(callback)
            callback(_res);
    });
};

boObj.upsertCatalog = (_cat,_playList,_device ,callback) => {
    let defCatTemplate = {
        display_name : _playList.name,
        image : _.find(_playList.images, {width: 300})
    };
    let bar = session.get("bar");
    console.log(bar);
    _cat = _cat||defCatTemplate;
    _cat.playList = _playList;
    _cat.barId = bar._id;
    catDao.upsertCatalog(_cat, _res => {
        callback(_res);
        console.log("Init Importing Tracks");
        
    });
};

boObj.flushCatalog = (_cat, callback) => {
   
};


module.exports =  boObj;