let request = require("request");
let _ = require("underscore");

const paths = {endpoint: 'https://api.spotify.com',
               access:'https://accounts.spotify.com/api/token'};

let clientConf;
const init = (_conf) => {
    clientConf = _conf;
};

const getUsrAccessToken = (_code, callback) => {
    let basicClientAuth =  Buffer.from(clientConf.clientId+":"+clientConf.clientSecret).toString('base64');
    var options = {
        url: paths.access+"?grant_type=authorization_code&code="+_code+"&redirect_uri="+encodeURIComponent(clientConf.redirectUri),
        headers: {
          "Accept-Language": "en-US,en;q=0.5",
          "Content-Type":"application/x-www-form-urlencoded",
          "Authorization":"Basic "+basicClientAuth
        },
     };
     request.post(options, function (err, resp, body){
        if(err){
            callback({success:false,msg:"Failed getting token"});
            return ;
        }
         body = JSON.parse(body);
         if(body.error){
            callback(body);
            return ;
         }
         getBasicUserInfo(body.access_token, _usr => {
            _usr.session = body;
            callback({success:true,user: _usr});
         });
         
     });
};

const getBasicUserInfo = (access_token,callback) => {    
  let options = {
    url: paths.endpoint+"/v1/me",
    headers: {
      "Content-Type":"application/json",
      "Authorization":"Bearer "+access_token
    },
 };
  request.get(options,(err, resp, body) => {
    if(err){
        callback({failed:true});
        return ;
    }
    callback(JSON.parse(body));
  });   
};

const refreshToken = (refresh_token, callback) => {
  let _body = {
    "refresh_token": refresh_token 
  };
  let endpoint  = "/v1/me/playlists";
  let opcs = {method:"POST",body:_body};
  apiCall(endpoint,opcs, _res => {
    if(callback)
      callback(_res);
  });
};


const apiCall = (service, opcs, callback) => {
    if(!service || !opcs.access_token){
        callback({success:false,msg:"Missed Parameters"});
        return ;
    }
    let options = {
        url: paths.endpoint+service,
        headers: {
          "Content-Type":opcs.contentType||"application/json",
          "Authorization":"Bearer "+opcs.access_token
        },
        method: opcs.method||"GET"
     };
     if(opcs.body)
        options.body = JSON.stringify(opcs.body);

     if(opcs.headers){
        options.headers = _.extend(options.headers, opcs.headers);
     }
     console.log("Getting Playlists", options);
     request(options,(err, resp, body) => {
        if(err){
            callback({failed:true,msg:"Failed call to API"});
            return ;
        }
        try {
          callback(JSON.parse(body));
        } catch (error) {
          console.log(error, resp);
          
          callback({err:err});
        }
        
     });   

};


//Playlist calls
let api = {};
api.getAllUserPlaylist = (sess, callback) => {   
  let endpoint  = "/v1/me/playlists";
  let opcs = {"access_token":sess};
  apiCall(endpoint,opcs, _res => {
      callback(_res);
  });
};

api.getDevices = (sess, callback) => {
      let endpoint  = "/v1/me/player/devices";
      let opcs = {"access_token":sess,method:"GET"};
      apiCall(endpoint,opcs, _res => {
          callback(_res);
      });
};

api.createSpotcloudPlaylist = (devName, sess, callback) => {
    let plData = {
        "name": devName+"-Spotcloud-Playlist",
        "description": "On this playlist other users will schedule tracks",
        "public": false
      };
      let endpoint  = "/v1/me/playlists";
      let opcs = {"access_token":sess,method:"POST",body:plData};
      apiCall(endpoint,opcs, _res => {
        if(callback)
          callback(_res);
      });
};


api.getTracksPerPlayList = (sess,plId, callback) => {
  let endpoint  = "/v1/playlists/"+plId+"/tracks";
  let opcs = {"access_token":sess,method:"GET"};
  apiCall(endpoint,opcs, _res => {
      callback(_res);
  });
};

//-------------------------

module.exports =  {
    init:init,
    getUsrAccessToken: getUsrAccessToken,
    apiCall: apiCall,
    refreshToken:refreshToken,
    api:api
};