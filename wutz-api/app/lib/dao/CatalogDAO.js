let db = require("./MongoDAO");
let _ = require("underscore");
let datObj = {};

datObj.getBarEventForUser = (_user, callback) => {
    db.getDb(dbuser => {
        dbuser.collection("BarEvent").find({email:_user.email})
        .toArray((err,arr) => {
            if(err){
                console.log(err);
                return ;
            }
            console.log(arr);
            let res;
            if(arr.length <= 0){
                callback({no_bar:true});
            }
            else
                res = arr[0];

            callback(res);
        });
    });
};

datObj.getCatalogsForBar = (_bar, callback) => {
    let barId = db.newObjId(_bar._id);
    db.getDb(dbuser => {
        dbuser.collection("Catalog").find({barId:barId})
        .toArray((err,arr) => {
            if(err){
                console.log(err);
                return ;
            }
            callback(arr);
        });
    });
};

datObj.upsertCatalog = (_catalog, callback) => {    
    let catId = db.newObjId(_catalog._id);
    delete _catalog._id;
    db.getDb(_db=>{
        _db.collection("Catalog")
        .findOneAndUpdate({_id:catId}, 
            { $set: _catalog },
            {upsert:true,returnNewDocument:true},             
            (err, doc) =>{
            if (err){ 
               callback({not_updated:true,error:err});
               return ;
            }

            callback(doc);
        });
    });
};

datObj.updateBarEvent = (_barEvent, callback) => {
    db.getDb(_db=>{
        _db.collection("BarEvent")
        .findOneAndUpdate({barevId:_barEvent.barevId}, 
            { $set: _barEvent },
            {upsert:true,returnNewDocument:true},             
            (err, doc) =>{
            if (err){ 
               callback({not_updated:true,error:err});
               return ;
            }
            callback(doc);
        });
    });
};

datObj.flushCatalog = (_deviceId, callback) => {
    
};

datObj.flushAlbums = (_deviceId, callback) => {
    
};

datObj.flushTracks = (_deviceId, callback) => {
    
};

module.exports =  datObj;