const tunnel = require("tunnel-ssh");
const mongoose = require("mongoose");
const fs = require("fs");
let ObjectId = require('mongoose').Types.ObjectId; 

let conf = JSON.parse(fs.readFileSync(__dirname+'/../../../config/default.json'));
let mongoConfig = conf.mongodbs;
let tunnelConfig = conf.sshTunnel;

let bluezincUserdb,wutzdb;

const connectWithTunnel = (callback) => {
    tunnel(tunnelConfig, (error, server) => {
        if(error){
            console.log("SSH connection error: " + error);
        }
       // console.log(server);
       mongoose.connect(mongoConfig.wutz, { useNewUrlParser: true });
       wutzdb = mongoose.connection;
       wutzdb.on('error', console.error.bind(console, 'connection error:'));
       wutzdb.once('open', () => {
                console.log("database connection Wutz");
               // wutzdb = mongoose.connection.useDb('Wutz');
              // console.log(wutzdb.db.collection("BarEvent").find({}));
              wutzdb.db.listCollections().toArray(function (err, collectionNames) {
                if (err) {
                  console.log(err);
                  return;
                }
                  console.log(collectionNames);
                  wutzdb.close();
              });
            
                /** 
                wutzdb.db.collection("BarEvent").find({}).limit(100)
                .next(_res =>{
                    console.log(_res);
                });
                **/
                callback(wutzdb);
       });       
    });
};

const connectNoTunnel = (callback) => {
       mongoose.connect(mongoConfig.wutz, { useNewUrlParser: true });
       wutzdb = mongoose.connection;
       wutzdb.on('error', console.error.bind(console, 'connection error:'));
       wutzdb.once('open', () => {
        console.log("database connection Wutz");
        if(callback)
            callback();
       }); 
       return wutzdb;
};

const getDb = (_conn, callback) => {
    if(typeof _conn === "function"){
        callback = _conn;
        _conn = null;
    }    
    if(wutzdb){
        if(_conn)
            wutzdb = wutzdb.useDb(_conn);
        else
            wutzdb =  wutzdb.useDb("Wutz");

        callback(wutzdb.db);
        return ;
    }
    
    console.log("Connecting ...");
    connectNoTunnel(()=>{
        getDb(_conn, callback);
    });
};

const getDbSync = (_conn) => {
    if(wutzdb){
        if(_conn)
            wutzdb = wutzdb.useDb(_conn);
        else
            wutzdb =  wutzdb.useDb("Wutz");

        return wutzdb.db;        
    }    
    console.log("Connecting ...");
    return connectNoTunnel(_conn);
};

const newObjId = (val) => {
    if(!val)
        return new ObjectId();
    return new ObjectId(val);
};


module.exports =  {
    getDb:getDb,
    getDbSync:getDbSync,
    newObjId:newObjId
};