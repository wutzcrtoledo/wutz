let db = require("./MongoDAO");
let _ = require("underscore");
let mom = require("moment");

const upsertUser = (_user, callback) => {
    db.getDb("BluezincGlobal",dbuser => {
        _user.lastModified = mom().format("x");
        dbuser.collection("User")
        .findOneAndUpdate({email:_user.email}, 
                          { $set: _user },
                          {upsert:true,returnNewDocument:true}, 
           (err, doc) =>{
                if (err){ 
                callback({not_updated:true,error:err});
                return ;
                }
                callback(doc);
        });


/** 
        dbuser.collection("User").find({id:_user.id})
        .toArray((err,arr) => {
            if(err){
                console.log(err);
                return ;
            }
            console.log(arr);
            let res;
            if(arr.length <= 0){
                dbuser.collection("User").insertOne(_user);
                res = _user;
            }
            else
                res = arr[0];

            callback(res);
        });
    **/
    });
};

module.exports =  {
    upsertUser:upsertUser
};