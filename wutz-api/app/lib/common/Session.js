let sessObj = {};
let sessions = {};

sessObj.startSession = (_sesOb) => {
    sessions = _sesOb;
};

sessObj.get = (_key) => {
        return sessions[_key]||{not_found:true};
};

sessObj.set = (_key, _value) => {
    sessions[_key] = _value;    
};



module.exports = sessObj;