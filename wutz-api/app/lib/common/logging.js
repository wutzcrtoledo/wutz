const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const os=require("os");
const fs = require("fs");
let conf =  JSON.parse(fs.readFileSync(__dirname+'/../../../config/default.json'));

require('winston-daily-rotate-file');



const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

//const files = new (transports.File)({ filename: __dirname+'/../../../exp/sse_out.log'});
const myconsole = new (transports.Console)();

const files = new (transports.DailyRotateFile)({
  filename: conf.logsPath+'/'+conf.appName+'-%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: false,
  maxSize: '20m',
  maxFiles: '14d'
});


let v_env = 'prod';

const logger = createLogger({
  format: combine(
    label({ label: '['+v_env+']' }),
    timestamp(),
    myFormat
  ),
  transports: [myconsole, files]
});

module.exports = function () {
  return logger;
};